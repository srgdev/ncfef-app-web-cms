<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Directory API
 */

require APPPATH.'/libraries/REST_Controller.php';

class Dir extends REST_Controller {

    /**
     * Responds to GET requests under /dot/
     */
    function DOT_get() {
        //$query = $this->db->query("SELECT * FROM `Directory_DOT`");
        //$res = $query->result_array();
        $res = array(1 => 'Hello');
        if($res) {
            $this->response($res, 200); // 200 being the HTTP response code
        } else {
            $this->response(array('error' => 'Directory listing for DOT could not be found'), 404);
        }
    }


}