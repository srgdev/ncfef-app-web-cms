function alignMiddle(){
	var wrap = $('.wrap');
	var wrapH = wrap.outerHeight();
	var bodyH = $('body').outerHeight();
	var winH = $(window).height();

	var mTop = (bodyH-wrapH)/2;
	var bMTop = (winH - bodyH)/2;

	wrap.css('margin-top', mTop);

	if(bMTop > 0){
		$('body').css('margin-top', bMTop);
	}
}

$(document).ready(function(){
	alignMiddle();

	$(window).resize(function(){
		alignMiddle();
	})
});