<?php

/**
 * Class Excel
 * Model for loading, parsing, and saving Excel spreadsheets to the database
 */
Class Excel extends CI_Model
{

    /**
     * Constructor also includes a call the PHPExcel set of classes and libraries, stored in a subfolder
     */
    function __construct() {
        parent::__construct();
        require(dirname(__FILE__).'/PHPExcel/PHPExcel.php');
    }

    /**
     * Loads an excel/csv file handler ($file) and parses it into an array
     * @param $file
     * @return array
     */
    function load($file){
        // Get type
        $type = PHPExcel_IOFactory::identify($file);
        // Create reader
        $reader = PHPExcel_IOFactory::createReader($type);
        // Read as data only, avoid formatting
        $reader->setReadDataOnly(true);
        // Load file
        $excel = $reader->load($file);
        // Set up return array
        $excelArray = array();
        // Set the current sheet by sheet name
        $worksheet = $excel->setActiveSheetIndex(0);
        // Prep work
        $maxr = $worksheet->getHighestRow();
        $maxc = $worksheet->getHighestColumn();
        $headings = $worksheet->rangeToArray('A1:'.$maxc.'1',null, true, true, true);
        $headings = $headings[1];
        $r = -1;
        // Loop through all rows
        for ($row = 2; $row <= $maxr; ++$row) {
            $dataRow = $worksheet->rangeToArray('A'.$row.':'.$maxc.$row,null, true, true, true);
            if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
                ++$r;
                foreach($headings as $columnKey => $columnHeading) {
                    if($columnHeading != ''){
                        $excelArray[$r][$columnHeading] = $dataRow[$row][$columnKey];    
                    }
                }
            }
        }
        return $excelArray;
    }

    /**
     * Creates a temporary table, prefixed with 'temp_', and named according to the current sheet ($name)
     * @param $headers
     * @param $name
     * @return mixed
     */
    function createTable($headers, $name){
        if($this->db->table_exists($name)){
            $this->db->query('DROP TABLE '.$name);
        };
        $query = 'CREATE TABLE '.$name.' (';
        foreach($headers as $index=>$colname){
            $query .= '`'.$colname.'` TEXT, ';
        }
        $query = rtrim($query, ', ').')';
        return $this->db->query($query);
    }

    /**
     * Inserts all 'rows' of data into the table of choice (temp)
     * @param $data
     * @param $name
     * @return bool
     */
    function insertData($data, $name){
        $headers = $this->getHeaders($data);
        $tableNowExists = $this->createTable($headers, $name);
        $return = $tableNowExists;
        if($tableNowExists){

            foreach($data as $index=>$entry){
                foreach($entry as $index=>$value){
                    if(!(strpos(strtolower($index), 'appid') === false) || !(strpos(strtolower($index), 'photoid') === false)){
                        $filename = $value.'.jpg';
                        if(file_exists('../images/photos/'.$filename)){
                            $entry[] = 'images/photos/'.$filename;
                        } else {
                            $entry[] = 'images/photos/na.jpg';
                        }
                        if(file_exists('../images/photos/small/'.$filename)){
                            $entry[] = 'images/photos/small/'.$filename;
                        } else {
                            $entry[] = 'images/photos/small/na.jpg';
                        }
                    }
                    $entry[$index] = mysql_real_escape_string($entry[$index]);
                }
                $colheaders = implode($headers, "`, `");
                $values = implode($entry, "', '");
                $query = "INSERT INTO ".$name." (`".$colheaders."`) VALUES ('".$values."')";
                if(!$this->db->query($query)){
                    $return = false;
                };
            }
        }
        return $return;
    }

    function getHeaders($data){
        $headers = array_keys($data[0]);
        foreach($data[0] as $index=>$value){
            if(!(strpos(strtolower($index), 'appid') === false) || !(strpos(strtolower($index), 'photoid') === false)){
                if(!(strpos(strtolower($index), 'appid') === false)){
                    $prefix = substr($index, 0, strpos(strtolower($index), 'appid'));
                } else if (!(strpos(strtolower($index), 'photoid') === false)){
                    $prefix = substr($index, 0, strpos(strtolower($index), 'photoid'));
                }
                $headers[] = $prefix.'ImageUrl';
                $headers[] = $prefix.'SmallImageUrl';
            }
        }
        foreach($headers as $index=>$colname){
            $headers[$index] = str_replace(' ', '', $colname);
        }
        return $headers;
    }

    /**
     * The main conversion function; loops through all sheets in a file, adds them to temp tables, returns false on error
     * @param $file
     * @return bool
     */
    function excelToDatabase($file){
        $data = $this->load('./uploads/'.$file);
        $name = $this->get_name($file);

        $valid_spreadsheet_names = array(
            'directory_cabinet',
            'directory_congress',
            'directory_house',
            'directory_judicial',
            'directory_senate',
            'directory_us_congress',
            'general_election_data_congress',
            'general_election_data_council',
            'general_election_data_house',
            'general_election_data_judicial',
            'general_election_data_senate',
            'general_election_data_ussenate'
        );

        if(!in_array($name, $valid_spreadsheet_names)){
            return false;
        }

        if(!$this->insertData($data, $name)){
            return false;
        } else {
            $this->add_record($name, $file);
            return true;
        }
    }

    /**
     * Returns name free from extension and datestamp
     * @param $file
     * @return mixed
     */
    function get_name($file){
        $parts = explode('.', $file);
        $name = $parts[0];
        $name = preg_replace('/_[0-1]{1}[0-9]{1}[0-3]{1}[0-9]{1}[0-1]{1}[0-9]{1}/', '', $name);
        $name = strtolower($name);
        return $name;
    }

    /**
     * Adds an update record to the database for a named file and associated table
     * @param $name
     * @param $file
     */
    function add_record($name, $file){
        $this->db->query("INSERT INTO updates (`table`, `file`) VALUES ('$name', '$file')");
    }
}