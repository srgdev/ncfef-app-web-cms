'use strict';

/* Controllers */

almanac.controller('home', ['$scope', '$state', 'Data', 'Linking', function($scope, $state, Data, Linking){

    if(Data.rawdata.menu.districts_nchouse){
        $scope.house = true;
    }
    if(Data.rawdata.menu.districts_ncsenate){
        $scope.senate = true;
    }
    if(Data.rawdata.menu.districts_congress){
        $scope.congress = true;
    }
    if(Data.rawdata.menu.directories){
        $scope.directories = true;
    }
    if(Data.rawdata.candidates){
        $scope.candidates = true;
    }
    if(Data.rawdata.menu.districts_ussenate){
        $scope.ussenate = true;
    }
    if(Data.rawdata.menu.judicial){
        $scope.judicial = true;
    }

    if(Data.rawdata.menu.districts_council){
        $scope.council = true;
    }
    $.each(Data.rawdata.menu.directories, function(i, v){
        if(v['name'] == 'Cabinet'){
            $scope.cabinet = true;
        }
    });

    $scope.getDistrictLink = function(type){
        return $state.href("districts", { districttype: type });
    }

}])