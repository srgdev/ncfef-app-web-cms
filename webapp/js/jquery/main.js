$(document).ready(function(){
    function uiInit(){
        $('#menuToggle').click(function(e){
            e.preventDefault();
            $('.blur').css('height', $('body').outerHeight() + 100);
            $('.mainMenu').css('height', $(window).height());
            $('#wrapper').css('overflow', 'hidden').css('height', $(window).height());
            $('.mainMenu, .blur').toggleClass('active');
            $('.closeButton, .blur').unbind();
            $('.closeButton, .blur').click(function(e){
                e.preventDefault();
                $('#wrapper').css('height', '100%').css('overflow', 'visible');
                $('.mainMenu, .blur').toggleClass('active');
            })
        })



        $('.menuItem a').click(function(e){
            var link = $(this);
            var listItem = link.parent();
            if(link.siblings('ul').length > 0){
                e.preventDefault();
                listItem.toggleClass('active');
                link.siblings('ul').toggleClass('active');
                link.siblings('.fa').toggleClass('active');
                if(listItem.siblings('.menuItem.active').length == 0){
                    $('.menuSubTitle, .menuTitle').toggleClass('active');
                }
            } else {
                $('#wrapper').css('height', '100%').css('overflow', 'visible');
                $('.mainMenu, .blur').toggleClass('active');
            }
        });

        $(window).resize(function(){
            $('.blur').css('height', $('body').outerHeight()) + 100;
            $('.mainMenu').css('height', $(window).height());
        })

        $('.menuSubTitle').click(function(e){
            $('.menuItem.active > a').click();
        })


        $(document).on('touchmove',function(e){
            e.preventDefault();
        });

        //uses body because jquery on events are called off of the element they are
        //added to, so bubbling would not work if we used document instead.
        $('body').on('touchstart','.scrollable',function(e) {
            if (e.currentTarget.scrollTop === 0) {
                e.currentTarget.scrollTop = 1;
            } else if (e.currentTarget.scrollHeight === e.currentTarget.scrollTop + e.currentTarget.offsetHeight) {
                e.currentTarget.scrollTop -= 1;
            }
        });

        //prevents preventDefault from being called on document if it sees a scrollable div
        $('body').on('touchmove','.scrollable',function(e) {
            e.stopPropagation();
        });

    }

    uiInit();

});