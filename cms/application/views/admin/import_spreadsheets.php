<?php
/**
 * Page for importing select spreadsheets to database.  Should only do once at a time.
 */
?>

<?php if(isset($importfiles)): ?>
    <?php if(@$importfiles['success']): ?>
        <div class="alert alert-success"><?php echo $importfiles['success']; ?></div>
    <?php elseif (@$importfiles['danger']): ?>
        <div class="alert alert-danger"><?php echo $importfiles['danger']; ?></div>
    <?php endif; ?>
<?php endif; ?>

<form action="<?php echo base_url('index.php/admin/import_spreadsheets'); ?>" method="post">
    <div class="form-group">
        <?php if(isset($files) || !empty($files)): ?>
            <select name="file" class="form-control tall" size="<?php echo count($files); ?>">
                <?php foreach($files as $file): ?>
                    <option value="<?php echo $file; ?>"><?php echo $file; ?></option>
                <?php endforeach; ?>
            </select>
        <?php endif; ?>
    </div>
    <div class="form-group">
        <input class="btn btn-md btn-primary" type="submit" value="import spreadsheet" />
    </div>
</form>