<?php
/**
 * Admin login page - loads as a solo landing page to login, avoiding complications with sidebar, etc.
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/bootstrap.min.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/bootstrap-theme.min.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/layout.css'); ?>" />

    <title><?php echo $title; ?></title>
</head>
<body class="login">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please sign in</h3>
                    </div>
                    <div class="panel-body">
                        <form class="form-signin" role="form" action="<?php echo base_url('index.php/admin/login'); ?>" method="post">
                            <?php if(validation_errors()): ?>
                                <div class="alert alert-danger" role="alert"><?php echo validation_errors(); ?></div>
                            <?php endif; ?>
                            <input name="username" type="text" class="form-control" placeholder="Username" required="" autofocus="">
                            <br>
                            <input name="password" type="password" class="form-control" placeholder="Password" required="">
                            <br>
                            <button class="btn btn-md btn-primary btn-block" type="submit">Log in</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script src="<?php echo base_url('js/jquery-1.10.2.js'); ?>"></script>
<script src="<?php echo base_url('js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('js/main.js'); ?>"></script>
</body>
</html>