<?php
/**
 * The admin footer file, loads all js and closes tags
 * For admin pages
 */
?>
            </div>
        </div>
    </div>



<script src="<?php echo base_url('js/jquery-1.10.2.js'); ?>"></script>
<script src="<?php echo base_url('js/bootstrap.min.js'); ?>"></script>

<!-- WYSIWYG Editor -->
<script src="<?php echo base_url('js/external/jquery.hotkeys.js'); ?>"></script>
<script src="<?php echo base_url('js/external/google-code-prettify/prettify.js'); ?>"></script>
<script src="<?php echo base_url('js/bootstrap-wysiwyg.js'); ?>"></script>

<script src="<?php echo base_url('js/main.js'); ?>"></script>

</body>
</html>