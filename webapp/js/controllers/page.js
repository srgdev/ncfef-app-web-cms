'use strict';

/* Controllers */

almanac.controller('page', ['$scope', '$stateParams', '$sce', function($scope, $stateParams, $sce){

    $scope.page = almanacData.pages[$stateParams.id];

    $scope.content = $sce.trustAsHtml($scope.page.content);

}]);