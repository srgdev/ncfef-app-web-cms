<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Admin
 * Loads admin pages, handles user access to admin panel
 */
class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();

        ini_set('memory_limit','128M');

        $this->load->model('user');
        $this->load->model('Excel');

        $this->load->library('session');
    }

    /**
     * Loads default view, if logged in, or login page if not
     */
    function index() {
        $data['title'] = 'Dashboard';
        if($this->session->userdata('logged_in')){
            $data['user'] = $this->session->userdata('logged_in');
            $data['updates'] = $this->get_updates_array();
            $this->load->view('admin/header', $data);
            $this->load->view('admin/index', $data);
            $this->load->view('admin/footer');
        } else {
            $this->load->helper(array('form'));
            $this->load->view('admin/login_view', $data);
        }
    }

    /**
     * Checks posted vars and validates them against database, then redirects or loads view with error message
     */
    function login(){
        $data['title'] = 'Login!';
        //This method will have the credentials validation
        $this->load->library('form_validation');
        // Set form validation rules and callback validation filtering
        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
        if($this->form_validation->run() == FALSE) {
            //Field validation failed. User redirected to login page
            $data['title'] .= '-Error!';
            $this->load->view('admin/login_view', $data);
        } else {
            //Go to private area
            redirect('admin', 'refresh');
        }
    }

    /**
     * Destroys session, redirect to login page
     */
    function logout() {
        $this->session->unset_userdata('logged_in');
        redirect('admin', 'refresh');
    }

    /**
     * Upload view
     */
    function upload() {
        if($this->session->userdata('logged_in')){
            $data['user'] = $this->session->userdata('logged_in');
            $data['title'] = 'Upload Spreadsheets';
            $uploading = $this->input->post('uploading');
            echo $uploading;
            if($uploading == 'true'){
                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'xlsx|csv|xls';
                $config['max_size']	= '0';
                $config['overwrite'] = TRUE;
                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload()) {
                    $data['error'] = $this->upload->display_errors();
                } else {
                    $filedata = $this->upload->data();
                    $filename = $filedata['file_name'];
                    $data['success'] = 'Successfully loaded '.$filename;
                }
            }
            $this->load->view('admin/header', $data);
            $this->load->view('admin/upload', $data);
            $this->load->view('admin/footer');
        } else {
            redirect('admin', 'refresh');
        }
    }

    /**
     * filter callback to check password against database
     * @param $password
     * @return bool
     */
    function check_database($password) {
        //Field validation succeeded.&nbsp; Validate against database
        $username = $this->input->post('username');
        //query the database
        $result = $this->user->login($username, $password);
        if($result) {
            $sess_array = array();
            foreach($result as $row) {
                $sess_array = array(
                    'id' => $row->id,
                    'username' => $row->username
                );
                $this->session->set_userdata('logged_in', $sess_array);
            }
            return TRUE;
        } else {
            $this->form_validation->set_message('check_database', 'Invalid username or password');
            return false;
        }
    }

    /**
     * View to manage uploads
     * @uses get_filenames()
     */
    function manage_uploads() {
        if($this->session->userdata('logged_in')){
            $data['user'] = $this->session->userdata('logged_in');
            $data['title'] = 'Manage Uploads';
            $files = $this->input->post('files');
            if(isset($files) && !empty($files)){
                $data['rmfiles'] = array();
                foreach ($files as $path) {
                    $fpath = './uploads/'.$path;
                    if (file_exists($fpath)) {
                        if(unlink($fpath)){
                            $data['rmfiles']['success'][] = $path;
                        } else {
                            $data['rmfiles']['danger'][] = $path;
                        }
                    } else {
                        $data['rmfiles']['danger'][] = $path;
                    }
                }
            }
            $data['files'] = get_filenames('./uploads/');
             usort($data['files'], function($a, $b){
                $a = preg_replace("/[^0-9]/", "", $a);
                $b = preg_replace("/[^0-9]/", "", $b);

                $a = substr($a, 4, 2) . substr($a, 0, 2) . substr($a, 2, 2);
                $b = substr($b, 4, 2) . substr($b, 0, 2) . substr($b, 2, 2);

                return $a < $b;
            });
            $this->load->view('admin/header', $data);
            $this->load->view('admin/manage_uploads', $data);
            $this->load->view('admin/footer');
        } else {
            redirect('admin', 'refresh');
        }
    }

    /**
     * Import spreadsheets view
     */
    function import_spreadsheets() {
        if($this->session->userdata('logged_in')){
            $data['user'] = $this->session->userdata('logged_in');
            $data['title'] = 'Import Spreadsheets';
            $toimport = $this->input->post('file');
            if(isset($toimport) && !empty($toimport)){
                $imported = $this->import_spreadsheet($toimport);
                if($imported){
                    $data['importfiles']['success'] = 'Successfully imported '.$toimport.' to database.';
                } else {
                    $data['importfiles']['danger']  = 'Error importing '.$toimport . ' - Please check your file name and column names.';
                }
            }
            $data['files'] = get_filenames('./uploads/');
            usort($data['files'], function($a, $b){
                $a = preg_replace("/[^0-9]/", "", $a);
                $b = preg_replace("/[^0-9]/", "", $b);

                $a = substr($a, 4, 2) . substr($a, 0, 2) . substr($a, 2, 2);
                $b = substr($b, 4, 2) . substr($b, 0, 2) . substr($b, 2, 2);

                return $a < $b;
            });
            
            $this->load->view('admin/header', $data);
            $this->load->view('admin/import_spreadsheets', $data);
            $this->load->view('admin/footer');
        } else {
            redirect('admin', 'refresh');
        }
    }

    function sort_by_data_suffix($a, $b){

    }

    /**
     * Takes a file and sends it the Excel Model for processing and db insertion
     * @param string $file
     * @return bool
     */
    function import_spreadsheet($file = '') {
        if($file != ''){
            return $this->Excel->excelToDatabase($file);
        } else {
            return FALSE;
        }
    }

    /**
     * Gets updates info
     * @return mixed
     */
    function get_updates_array(){
        $query = $this->db->query("SELECT * FROM updates ORDER BY date DESC");
        return $query->result();
    }

    /**
     * Shows list of current pages, buttons for creating/editing
     */
    function pages(){

        if($this->session->userdata('logged_in')){

            $data['title'] = 'App Custom Pages';
            $data['pages'] = $this->page->get_all();
            $data['user'] = $this->session->userdata('logged_in');

            $this->load->view('admin/header', $data);
            $this->load->view('admin/pages', $data);
            $this->load->view('admin/footer');

        } else {
            redirect('admin', 'refresh');
        }
    }

    function add_page(){
        if($this->session->userdata('logged_in')){

            $data['title'] = 'Create Page';
            $data['user'] = $this->session->userdata('logged_in');

            $this->load->view('admin/header', $data);
            $this->load->view('admin/create_page', $data);
            $this->load->view('admin/footer');

        } else {
            redirect('admin', 'refresh');
        }
    }

    function create_page(){
        if($this->session->userdata('logged_in')){

            $page = $this->input->post(null, true);
            $id = $this->page->insert($page);

            if($id){
                $this->Excel->add_record('Pages', $page['title']);
                $this->session->set_flashdata('success', 'Page created');
                redirect('admin/edit_page/'.$id, 'refresh');
            } else {
                $this->session->set_flashdata('error', 'Page could not be created');
                redirect('admin/add_page/', 'refresh');
            }

        } else {
            redirect('admin', 'refresh');
        }
    }

    function edit_page($id){
        if($this->session->userdata('logged_in')){

            $data['title'] = 'Edit Page';
            $data['page'] = $this->page->get($id);
            $data['user'] = $this->session->userdata('logged_in');

            if($data['page']){
                $this->load->view('admin/header', $data);
                $this->load->view('admin/edit_page', $data);
                $this->load->view('admin/footer');
            } else {
                $this->session->set_flashdata('error', 'Page could not be found');
                redirect('admin/pages/', 'refresh');
            }

        } else {
            redirect('admin', 'refresh');
        }
    }

    function update_page($id){
        if($this->session->userdata('logged_in')){

            $page = $this->input->post(null, true);
            $updated = $this->page->save($page, $id);

            if($updated){
                $this->Excel->add_record('Pages', $page['title']);
                $this->session->set_flashdata('success', 'Page updated');
            } else {
                $this->session->set_flashdata('error', 'Page could not updated');
            }
            redirect('admin/edit_page/'.$id, 'refresh');

        } else {
            redirect('admin', 'refresh');
        }
    }

    function upload_images(){
        if($this->session->userdata('logged_in')){

            $data['user'] = $this->session->userdata('logged_in');
            $data['title'] = 'Upload Photos';

            $uploading = $this->input->post('uploading');

            if($uploading == 'true'){

                if (!file_exists('./image_uploads')) {
                    mkdir('./image_uploads', 0755, true);
                }

                $config['upload_path'] = './image_uploads/';
                $config['allowed_types'] = 'zip';
                $config['max_size'] = '0';
                $config['overwrite'] = TRUE;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload()) {
                    $data['error'] = $this->upload->display_errors();
                } else {

                    $filedata = $this->upload->data();
                    $filename = $filedata['file_name'];

                    $this->load->model('Images');

                    $imported = $this->Images->import_zip('./image_uploads/' . $filename, $filename);

                    if(is_array($imported)){

                        $data['success'] = 'Successfully imported and unzipped '.$filename . '<br>';    

                        foreach($imported as $size=>$bools){
                            foreach($bools as $bool=>$file){
                                foreach($file as $filename=>$message){
                                    if(!isset($data[$bool])){
                                        $data[$bool] = '';
                                    }

                                    $data[$bool] .= 'File ' . $filename . ': ' . $message . '<br>';
                                }
                            }
                        }

                    } else {
                        $data['error'] = 'Couldn\'t import and unzip '.$filename;
                    }
                    
                }
            }

            $this->load->view('admin/header', $data);
            $this->load->view('admin/images', $data);
            $this->load->view('admin/footer');

        } else {
            redirect('admin', 'refresh');
        }
    }


}