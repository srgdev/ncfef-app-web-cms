'use strict';

/* Controllers */

almanac.controller('menu', ['$scope', '$state', 'Data', 'Linking', function($scope, $state, Data, Linking){
    // Get Raw data to repeat in menu
    $scope.menu = Data.rawdata.menu;
    $scope.candidates = Data.rawdata.candidates;
    // Assign Linking
    $scope.linking = Linking;
    // Hide the menu when clicking on a link, except when it links back to current state
    $scope.hideMenu = function(){
        if($('.mainMenu').hasClass('active')){
            $('#wrapper').css('height', '100%').css('overflow', 'visible');
            $('.mainMenu, .blur').toggleClass('active');
        }
    }
}]);