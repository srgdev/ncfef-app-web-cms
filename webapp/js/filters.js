'use strict';

/* Filters */

almanac.filter('byDist', function() {
    return function(candidates, type, district) {
        var returns = [];
        for(var i = 0; i < candidates.length; i++){
            var candidate = candidates[i];
            if(candidate.type == type && candidate.district == district){
                returns.push(candidate);
            }
        }
        return returns;
    };
});

almanac.filter('byType', function() {
    return function(districts, type) {
        var returns = [];
        for(var i = 0; i < districts.length; i++){
            var district = districts[i];
            if(district.type == type){
                returns.push(district);
            }
        }
        return returns;
    };
});

almanac.filter('meterTitle', function() {
    return function(meterAngle){
        switch(meterAngle){
            case -72:
                return 'Strong Democratic';
                break;
            case -36:
                return 'Leaning Democratic';
                break;
            case 0:
                return 'Swing';
                break;
            case 36:
                return 'Leaning Republican';
                break;
            case 72:
                return 'Strong Republican';
                break;
            default:
                return 'Voter Index';
                break;
        }
    }
})

almanac.filter('toPercent', function(){
    return function(meterVal){
        if(meterVal){
            if(meterVal.indexOf(',') > -1 || meterVal.indexOf('$') > -1){
                return meterVal;
            } else {
                if(meterVal.indexOf('%') > -1){
                    return Number(meterVal.replace('%', ''));
                } else {
                    return Number((Number(meterVal)*100).toFixed(2));
                }
            }
        } else {
            return meterVal;
        }

    }
})

almanac.filter('toSignedPercent', function(){
    return function(meterVal){
        if(meterVal){
            if(meterVal.indexOf(',') > -1 || meterVal.indexOf('$') > -1){
                return meterVal;
            } else {
                if(meterVal.indexOf('%') > -1){
                    return Number(meterVal.replace('%', '')) + '%';
                } else {
                    return Number((Number(meterVal)*100).toFixed(2)) + '%';
                }
            }
        } else {
            return 'unkown';
        }

    }
})

almanac.filter('toMoney', function(){
    return function(coh){
        if(coh != ''){
            coh = coh.replace('$', '');
            var x = coh.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return '$' + x1 + x2;
        } else {
            return '';
        }
    }
})

almanac.filter('distName', function(){
    return function(type){
        if(type == 'congress' || type=='us_congress'){
            return 'US Congress';
        } else if(type == 'ussenate' || type == 'us_senate'){
            return 'US Senate';
        } else {
            return 'NC ' + (type.charAt(0).toUpperCase() + type.slice(1));
        }
    }
})

almanac.filter('distType', function(){
    return function(district){
        if(district == null){
            return '';
        }else if(isNaN(parseFloat(district))){
            return '('+district+')';
        } else {
            return 'District ' + district;
        }
    }
})

almanac.filter('getAff', function(){
    return function(aff){
        return aff == 'D' ? 'Dem' : aff == 'L' ? 'Lib' : 'Rep';
    }
})

almanac.filter('getMeterAngle', function(){
    return function(cvb){
        switch(cvb){
            case '1':
                return -72;
                break;
            case '2':
                return -36;
                break;
            case '3':
                return 0;
                break;
            case '4':
                return 36;
                break;
            case '5':
                return 72;
                break;
            default:
                return 0;
                break;
        }
    }
})

almanac.filter('incumbency', function(){
    return function(incumbent){
        if(incumbent == 1){
            return '(Incumbent)';
        } else {
            return '';
        }
    }
})

almanac.filter('reverse', function() {
    return function(items) {
        return items ? items.slice().reverse() : items;
    };
});

almanac.filter('addPercent', function(){
    return function(val){
        if(val != '' && val){
            return val + '%';
        } else {
            return val;
        }
    }
})

almanac.filter('namedCounties', function(){
    return function(counties){
        var returns = [];
        angular.forEach(counties, function(val, index){
            if(counties[index].name){
                returns.push(counties[index]);
            }
        })

        return returns;
    }
})

almanac.filter('addDash', function(){
    return function(string){
        if(string != '' && string){
            return string + ' - ';
        }
    }
})

almanac.filter('dirName', function(){
    return function(listing){
        if(listing.type == 'cabinet'){
            return listing.Office;
        } else {
            if(listing.type == 'congress' || listing.type == 'judicial'){
                return listing.LeadershipPosition;
            } else if(listing.type == 'us_congress'){
                return listing.LeadershipPosition;
            } else if(listing.type == 'ussenate' || listing.type == 'us_senate'){
                return 'US Senate';
            } else {
                return 'NC ' + (listing.type.charAt(0).toUpperCase() + listing.type.slice(1));
            }
        }
    }
})