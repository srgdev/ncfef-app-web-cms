<?php

/**
 * Class Page
 * Data Model for Pages
 */
Class Page extends CI_Model
{

    function _construct(){
        parent::__construct();
    }

    public function get_all(){
        $query = $this -> db -> get('pages');
        return $query -> num_rows() > 0 ? $query->result() : false;
    }

    public function get($id){
        $query = $this->db->get_where('pages', array('id' => $id));
        return $query -> num_rows() == 1 ? $query->row() : false;
    }

    public function insert($page){
        if($this->db->insert('pages', $page)){
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function save($page, $id){

        // Checkbox for being published
        $page['published'] = isset($page['published']) ? 1 : 0;

        // Strip newlines from html
        $page['content'] = trim(preg_replace('/\s\s+/', ' ', $page['content']));

        $this->db->where('id', $id);
        return $this->db->update('pages', $page);
    }

}