'use strict';

/* Controllers */

almanac.controller('district', ['$scope', '$rootScope', '$filter', '$state', '$stateParams', 'Data', function($scope, $rootScope, $filter, $state, $stateParams, Data){


    $scope.candidates = Data.candidates({district: $stateParams.district}, {type: $stateParams.districttype}).get();
    $scope.district = Data.districts({number: $stateParams.district}, {type: $stateParams.districttype}).first();

    $scope.voterAff = [
        {
            value: $filter('toPercent')($scope.district.registered.dem),
            color:"#3f5366",
            highlight: "#3f5366",
            label: "Democrat"
        },
        {
            value: $filter('toPercent')($scope.district.registered.rep),
            color: "#171d25",
            highlight: "#171d25",
            label: "Republican"
        },
        {
            value: $filter('toPercent')($scope.district.registered.una),
            color: "#7ea5cc",
            highlight: "#7ea5cc",
            label: "Unaffliated"
        },
        {
            value: $filter('toPercent')($scope.district.registered.lib),
            color: "#5f7c99",
            highlight: "#5f7c99",
            label: "Libertarian"
        }
    ];
    $scope.voterRace = [
        {
            value: $filter('toPercent')($scope.district.voters.asian),
            color:"#3f5366",
            highlight: "#3f5366",
            label: "Asian"
        },
        {
            value: $filter('toPercent')($scope.district.voters.black),
            color: "#171d25",
            highlight: "#171d25",
            label: "Black"
        },
        {
            value: $filter('toPercent')($scope.district.voters.hispanic),
            color: "#7ea5cc",
            highlight: "#7ea5cc",
            label: "Hispanic"
        },
        {
            value: $filter('toPercent')($scope.district.voters.other),
            color: "#5f7c99",
            highlight: "#5f7c99",
            label: "Other"
        },
        {
            value: $filter('toPercent')($scope.district.voters.white),
            color: "#3f5366",
            highlight: "#3f5366",
            label: "White"
        }
    ];
    $scope.voterGender = [
        {
            value: $filter('toPercent')($scope.district.gender.f),
            color:"#3f5366",
            highlight: "#3f5366",
            label: "Female"
        },
        {
            value: $filter('toPercent')($scope.district.gender.m),
            color: "#171d25",
            highlight: "#171d25",
            label: "Male"
        }
    ];

    $scope.goToCand = function(type, appid){
        $state.go('candidate', {districttype: type, appid: appid});
    }

    $scope.goToNext = function(){
        var i = Number($scope.district.number) + 1;
        var distCount = Data.districts({type: $stateParams.districttype}).count();
        if(i > distCount){
            i = 1;
        }
        $state.go('district', {districttype: $scope.district.type, district: i})
    }

    $scope.goToPrev = function(){
        var i = Number($scope.district.number) - 1;
        var distCount = Data.districts({type: $stateParams.districttype}).count();
        if(i <= 0){
            i = distCount;
        }
        $state.go('district', {districttype: $scope.district.type, district: i})
    }

    var options = {
        segmentShowStroke : false,
        animationEasing : "easeInOutQuad",
        animationSteps : 20,
        percentageInnerCutout : 60
    };
    var ctx = $('.voterAff .chart')[0].getContext("2d");
    var voterAffChart = new Chart(ctx).Doughnut($scope.voterAff,options);
    var ctx1 = $('.voterRace .chart')[0].getContext("2d");
    var voterRaceChart = new Chart(ctx1).Doughnut($scope.voterRace,options);
    var ctx2 = $('.voterGend .chart')[0].getContext("2d");
    var voterGenderChart = new Chart(ctx2).Doughnut($scope.voterGender,options);

}]);