<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
/**
 * Class Email
 * Signs submission up to email list
 */
class Emailer extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    /**
     * Process submissions
     */
    function index() {

        if($_POST['email']){
            $email = trim($_POST['email']);
            if(filter_var($email, FILTER_VALIDATE_EMAIL)){
                $email = $email;
            } else {
                $email = null;
                $error['message'] = 'Invalid Email';
                $error['code'] = 100;
                echo json_encode($error);
                return;
            }
        } else {
            $error['message'] = 'Invalid Email';
            $error['code'] = 100;
            echo json_encode($error);
            return;
        }

        if($_POST['fname']){
            $fname = trim($_POST['fname']);
        } else {
            $error['message'] = 'Invalid First Name';
            $error['code'] = 100;
            echo json_encode($error);
            return;
        }

        if($_POST['lname']){
            $lname = trim($_POST['lname']);
        } else {
            $error['message'] = 'Invalid Last Name';
            $error['code'] = 100;
            echo json_encode($error);
            return;
        }

        if($_POST['phone']){
            $phone = trim($_POST['phone']);
        } else {
            $error['message'] = 'Invalid Phone';
            $error['code'] = 100;
            echo json_encode($error);
            return;
        }

        if($_POST['zip']){
            $zip = trim($_POST['zip']);
        } else {
            $error['message'] = 'Invalid ZIP';
            $error['code'] = 100;
            echo json_encode($error);
            return;
        }

        if($email && $fname && $lname && $phone && $zip){

            $to = 'cpdeclaissewalford@gmail.com';
            $subject = 'New Email Subscription Request from ' .$fname.' '.$lname.' ('.$email.')';
            $headers = 'From: <subscriptions(ncfefalmanac.org>' . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

            $message = '<html><body>';
            $message .= '<h1>Information</h1>';
            $message .= '<hr>';
            $message .= '<b>Name:</b> '.$fname.' '.$lname.'<br>';
            $message .= '<b>Email:</b> '.$email.'<br>';
            $message .= '<b>Phone:</b> '.$phone.'<br>';
            $message .= '<b>ZIP:</b> '.$zip.'<br>';
            $message .= '</body></html>';

            $mailed = mail($to, $subject, $message, $headers);

            if($mailed){
                $return['message'] = 'Success!';
                $return['code'] = 200;
                echo json_encode($return);
            } else {
                $return['message'] = 'Failed to email message!';
                $return['code'] = 100;
                echo json_encode($return);
            }

        }
    }

}