'use strict';

/* Controllers */

almanac.controller('candidateListDistrict', ['$scope', '$rootScope', '$state', '$stateParams', 'Data', 'Linking', function($scope, $rootScope, $state, $stateParams, Data, Linking){

    $scope.type = $stateParams.districttype;
    $scope.district = $stateParams.district;
    $scope.linking = Linking;
    // Get a preformatted array of all candidates from Service
    $scope.candidates = Data.candidatesArray;

}]);