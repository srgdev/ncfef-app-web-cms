'use strict';

/* Controllers */

almanac.controller('about', ['$scope', function($scope){

    $scope.formFilled = $.isEmptyObject(amplify.store('almanacForm'));

    $('.contactForm').submit(function(e){
        e.preventDefault();
        if($(this).parsley('validate')){
            var data = $(this).serialize();
            console.log(data);
            console.log(urlBase + '/emailer');
            $.post(urlBase + '/emailer', data, function(data){
                data = JSON.parse(data);
                if(data.code == 100){
                    $('.contactForm').prepend('<p class="error">Error: '+data.message+'</p>');
                } else if (data.code == 200){
                    var parent = $('.contactForm').parent();
                    $('.contactForm').slideUp(400, function(){
                        var message = $('<div style="display:none;" class="submitcircle"><span>Thank you for signing up!</span></h3>')
                        parent.append(message.slideDown(400, function(){
                            $('html, body').animate({
                                    scrollTop: $(document).height()-$(window).height()},
                                200,
                                "linear"
                            );
                        }));
                    });
                    amplify.store('almanacForm', data);
                } else {
                    $('.contactForm').prepend('<p class="error">There was an error processing your request.  Please try again later.</p>');
                }
            })
        }
    })

}]);