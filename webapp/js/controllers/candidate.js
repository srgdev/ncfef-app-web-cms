'use strict';

/* Controllers */

almanac.controller('candidate', ['$scope', '$rootScope', '$state', '$stateParams', 'Data', 'Linking', function($scope, $rootScope, $state, $stateParams, Data, Linking){

    // Query for Candidate
    if($stateParams.districttype == 'judicial'){
        $scope.candidate = Data.candidates({uniqueID: $stateParams.appid}).first();
    } else {
        $scope.candidate = Data.candidates({appid: $stateParams.appid}).first();
    }

    // Set up some incumbent only data
    if($scope.candidate.incumbent == '1'){
        // Query the directory for incumbent records
        if(Data.directory[$stateParams.districttype]){
            var candDirListing = Data.directory[$stateParams.districttype]({District: $scope.candidate.district}).first();
            if(candDirListing){
                // Assign a committees array and flag that this listing can be linked to directory
                $scope.candidate.committees = candDirListing.CommitteeAssignments;
                $scope.candidate.dirIndex = candDirListing.id;
            }
        }
    }

    // Assign Linking service out
    $scope.linking = Linking;

}]);