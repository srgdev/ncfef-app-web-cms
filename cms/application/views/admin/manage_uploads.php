<?php
/**
 * Page for managing files in the uploads folder.  Should only contain .xsls, .xsl, and .csv
 * TODO: Filtering for filetypes?  Maybe not.
 */
?>
<?php if(!empty($rmfiles)): ?>
    <?php if(!empty($rmfiles['success'])): ?>
        <div class="alert alert-success">Successfully deleted:<br>
        <?php foreach($rmfiles['success'] as $file): ?>
             <?php echo $file; ?><br>
        <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <?php if(!empty($rmfiles['danger'])): ?>
        <div class="alert alert-danger">Could not delete:<br>
            <?php foreach($rmfiles['danger'] as $file): ?>
                <?php echo $file; ?><br>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
<?php endif; ?>

<form action="<?php echo base_url('index.php/admin/manage_uploads'); ?>" method="post">
    <div class="form-group">
        <?php if(isset($files) || !empty($files)): ?>
            <select name="files[]" multiple class="form-control tall" size="<?php echo count($files); ?>">
                <?php foreach($files as $file): ?>
                    <option value="<?php echo $file; ?>"><?php echo $file; ?></option>
                <?php endforeach; ?>
            </select>
        <?php endif; ?>
    </div>
    <div class="form-group">
        <input class="btn btn-md btn-primary" type="submit" value="delete files" />
    </div>
</form>