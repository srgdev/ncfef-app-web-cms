<?php

/**
 * Class User
 * Handles logging in to admin section
 */
Class User extends CI_Model
{

    /**
     * Checks login values
     * @param $username
     * @param $password
     * @return bool
     */
    function login($username, $password) {
        $this -> db -> select('id, username, pass');
        $this -> db -> from('users');
        $this -> db -> where('username', $username);
        $this -> db -> where('pass', MD5($password));
        $this -> db -> limit(1);
        $query = $this -> db -> get();
        if($query -> num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }
}