<?php defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
/**
 * API
 */

/**
 * Require the rest controller dependency
 */
require APPPATH.'/libraries/REST_Controller.php';

/**
 * API controller and handler for incoming requests for app data
 * Also does some data parsing
 *
 */
class API extends REST_Controller {

    /**
     * Front facing url to append to all image strings (for application support without relative file paths)
     * @var string
     */
    public $frontUrl = '';


    /**
     * Define voting results fields here, no other proper dynamic way to do it since excel doesn't supper multidimensional arrays
     * @var array
     */
    public $voting_fields = array(
        '2014 Senate' => array(
            'd' => '14SenDem',
            'r' => '14SenRep',
            'l' => '14SenLIB'
        ),
        '2012 Presidential' => array(
            'd' => '12PresDem',
            'r' => '12PresRep',
            'l' => '12PresLib'
        ),
        '2012 Governor' => array(
            'd' => '12GovDem',
            'r' => '12GovRep',
            'l' => '12GovLib'
        ),
        '2012 Lt Governor' => array(
            'd' => '12LGovDem',
            'r' => '12LGovRep',

        ),
        '2012 Secretary of State' => array(
            'd' => '12SSDem',
            'r' => '12SSRep',

        ),
        '2012 Insurance' => array(
            'd' => '12InsDem',
            'r' => '12InsRep'
        ),
        '2012 Treasurer' => array(
            'd' => '12TreDem',
            'r' => '12TreRep'
        ),
        '2012 Auditor' => array(
            'd' => '12AudDem',
            'r' => '12AudRep'
        ),
        '2012 Agriculture' => array(
            'd' => '12AgrDem',
            'r' => '12AgrRep'
        ),
        '2012 Labor' => array(
            'd' => '12LabDem',
            'r' => '12LabRep'
        ),
        '2012 Pub. Inst.' => array(
            'd' => '12DPIDem',
            'r' => '12DPIRep'
        )
    );

    /**
     * Set custom fields for county information
     * @var array
     */
    public $county_fields = array(
        'UE Rate' => 'UE',
        'Public Jobs' => 'PUBLICJOBS',
        'Med HH Income' => 'MEDHHINCOME',
        'Own Home' => 'HOMEOWNER',
        'Col' => 'COLLEGE'
    );

    /**
     * Set active directories
     *@var array
     */
    public $directories = array(
        'cabinet', 
        'house', 
        'senate', 
        // 'congress', 
        'us_congress', 
        // 'ussenate', 
        'judicial'
    );

    /**
     * Set active districts
     *@var array
     */
    public $districts = array(
        'house', 
        'senate', 
        'congress', 
        'judicial',
        'ussenate',
        'council'
    );

    /**
     * Set a max number of counties to loop through (may change, so set in class scope)
     * @var int
     */
    public $max_counties = 12;

    /**
     * Set a max number of candidates to loop through
     * @var int
     */
    public $max_cands = 20;

    /**
     * Constructor
     */
    public function __construct(){
        parent::__construct();

        // Assign the front end URL from configuration, removeing the 'cms/' part
        $this->frontUrl = substr($this->config->item('base_url'), 0, -4);
    }

    /**
     * Main data return function
     * Calls together all data into large array, then responds with a 200 and JSON encoded data
     */
    function appdata_get($debug = false, $index = false){

        // Setup a data container
        $data = array();

        // Get menus, candidates,districts, directories, and pages
        $data['menu'] = $this->menu();
        $data['candidates'] = $this->candidates();
        $data['districts'] = $this->districts();
        $data['directory'] = $this->directories();
        $data['pages'] = $this->pages();

       if($debug){
            echo '<pre>';
            if($index){
                var_dump(@$data[$index]);
            } else {
                var_dump($data);
            }
            exit;
        }

        // Respond with a json encoded data stream
        $this->response($data, 200);
    }

    /**
     * Returns a string or integer of the last update time in ymdHis format
     * App then compares this to what it has and determines whether or not
     * to update the locally cached data
     */
    function lastupdate_get(){

        // Setup data payload
        $data = array();

        // Get the most recent update
        $date = $this->db->query("SELECT date FROM `updates` ORDER BY date DESC LIMIT 1")->row();

        // Convert to simple ymdHis format
        $lastupdate = date('ymdHis', strtotime($date->date));

        // Add to payload
        $data['lastupdate'] = $lastupdate;

        // JSON response
        $this->response($data, 200);
    }

    /**
     * Calls the defined array of directories and queries the database for them
     * Returns the entire table as an array
     * @return array
     */
    function directories() {

        $return = array();

        foreach($this->directories as $directory){
            if($this->db->table_exists('directory_'.$directory)){

                $rows = $this->db->query("SELECT * FROM `directory_$directory`")->result_array();

                if(!empty($rows)){
                    foreach($rows as $index=>$row){

                        // Create style arrays (for app use, ng-style)
                        $row['profile'] = $this->generate_style_array($row);
                        $row['small_profile'] = $this->generate_small_style_array($row);

                        // Push to rows array
                        $rows[$index] = $row;

                    }

                    // Push all rows to a directory index on the return array
                    $return[$directory] = $rows;
                }
            }
        }

        return $return;
    }

    /**
     * Uses predefined disrict types to query the database for existing tables.
     * Checks for valid values and returns
     * @return array
     */
    function districts() {

        $return = array();

        foreach($this->districts as $district){

            // Check if the district has a table associated with it
            if($this->db->table_exists('general_election_data_'.$district)) {

                // Parse the column name from the district name
                $distColName = '';
                switch($district){
                    case 'congress':
                        $distColName = 'CD';
                        break;
                    case 'house':
                        $distColName = 'NCHD';
                        break;
                    case 'senate':
                        $distColName = 'SD';
                        break;
                }

                // get election data for this districts
                $dist_rows = $this->db->query("SELECT * FROM `general_election_data_$district`")->result_array();

                if(!empty($dist_rows)) {

                    // Set up return array for this district type
                    $return[$district] = array();

                    // Loop through districts
                    foreach($dist_rows as $i=>$row){

                        // Get voting results for the district (to be used later)
                        $voting_results = array();
                        foreach($this->voting_fields as $name=>$fields){

                            // Parse the results
                            $voting_results[$name] = array();
                            foreach($fields as $index=>$field){

                                // Add the results of each type of voter to the overall results array
                                $voting_results[$name][$index] = isset($row[$field]) ? $row[$field] : null;
                            }
                        }

                        // set up the generic district details, including voters and demographics
                         $district_array = $this->create_general_district_info($district, $row, $distColName);

                         // Add voting results
                         $district_array['voting_results'] = $voting_results;

                        if($district == 'ussenate'){

                            // Append to main district array
                            $return[$district] = $district_array;

                        } elseif ($district == 'council'){

                            $district_array['smallimage'] = $this->frontUrl . 'images/maps/nc.jpg';
                            $district_array['largeimage'] = $this->frontUrl . 'images/maps/nc.jpg';
                            $district_array['profile'] = @$row['Candidate1Incumbent'] != '' ? $this->generate_style_array($row, 1) : array('background' => 'url('.$this->frontUrl . 'images/maps/nc.jpg) no-repeat center', 'background-size' => 'cover');
                            $district_array['small_profile'] = @$row['Candidate1Incumbent'] != '' ? $this->generate_small_style_array($row, 1) : array('background' => 'url('.$this->frontUrl . 'images/maps/nc.jpg) no-repeat center', 'background-size' => 'cover');

                            // Append to main district array
                            $return[$district][$i] = $district_array;

                        } elseif ($district == 'judicial'){

                            $district_array['number'] = $i;
                            $district_array['county_info'] = null;
                            $district_array['profile'] = @$row['Candidate1Incumbent'] != '' ? $this->generate_style_array($row, 1) : array('background' => 'url('.$this->frontUrl . 'images/maps/nc.jpg) no-repeat center', 'background-size' => 'cover');
                            $district_array['small_profile'] = @$row['Candidate1Incumbent'] != '' ? $this->generate_small_style_array($row, 1) : array('background' => 'url('.$this->frontUrl . 'images/maps/nc.jpg) no-repeat center', 'background-size' => 'cover');
                            $district_array['smallimage'] = @$row['Candidate1Incumbent'] != '' ? $this->generate_small_image_url($row, 1) : $this->frontUrl . 'images/maps/nc.jpg';
                            $district_array['largeimage'] = @$row['Candidate1Incumbent'] != '' ? $this->generate_small_image_url($row, 1) : $this->frontUrl . 'images/maps/nc.jpg';

                            // Append to main district array
                            $return[$district][$i] = $district_array;

                        } else {

                            // Finally, create the district main entry
                            $district_array['number'] =$row[$distColName];
                            $district_array['county_info'] = $this->generate_county_info($row);
                            $district_array['profile'] = @$row['Candidate1Incumbent'] != '' ? $this->generate_style_array($row, 1) : array('background' => 'url('.$this->frontUrl . 'images/maps/nc.jpg) no-repeat center', 'background-size' => 'cover');
                            $district_array['small_profile'] = @$row['Candidate1Incumbent'] != '' ? $this->generate_small_style_array($row, 1) : array('background' => 'url('.$this->frontUrl . 'images/maps/nc.jpg) no-repeat center', 'background-size' => 'cover');
                            $district_array['smallimage'] = $this->frontUrl . 'images/maps/'.$district.'_340/map_'.$row[$distColName].'.jpg';
                            $district_array['largeimage'] = $this->frontUrl . 'images/maps/'.$district.'_1000/map_'.$row[$distColName].'.jpg';

                            // Append to main district array
                            $return[$district][$i] = $district_array;
  
                        }

                        
                        
                    }
                }
            }
        }

        // Return the districts array
        return $return;
    }

    /**
     * Generate county-specific information from the district row
     * @param array district rows
     * @return array county information for particular district
     */
    private function generate_county_info($row){
        // Get County Demographics/info to append to main district array
        $county_info = array();
        for($i = 1; $i <= $this->max_counties; $i++){

            // If this county has an index
            if(isset($row[$i.'COUNTY'])){

                // Get custom fields
                $otherinfo = array();
                foreach($this->county_fields as $name=>$field){

                    // If this county has a valid county field as defined on our class
                    if(isset($row[$i.'COUNTY'.$field])){
                        $otherinfo[$name] = $row[$i.'COUNTY'.$field];
                    } else {
                        $otherinfo[$name] = null;
                    }
                }

                // Parse all other county info, formatting as need be
                $county_info[$i] = array(
                    'name' =>       $row[$i.'COUNTY'],
                    'percent' =>    (isset($row[$i.'DISTRICT%'])) ? round($row[$i.'DISTRICT%'], 2) : (isset($row[$i.'COUNTY%']) ? round($row[$i.'COUNTY%'], 2) : null),
                    'dem' =>        isset($row[$i.'COUNTYDEM']) ? round($row[$i.'COUNTYDEM'], 2) : null,
                    'rep' =>        isset($row[$i.'COUNTYREP']) ? round($row[$i.'COUNTYREP'], 2) : null,
                    'una' =>        isset($row[$i.'COUNTYUNA']) ? round($row[$i.'COUNTYUNA'], 2) : null,
                    'lib' =>        isset($row[$i.'COUNTYLIB']) ? round($row[$i.'COUNTYLIB'], 2) : null,
                    'black' =>      isset($row[$i.'COUNTYBLACK']) ? round($row[$i.'COUNTYBLACK'], 2) : null,
                    'white' =>      isset($row[$i.'COUNTYWHITE']) ? round($row[$i.'COUNTYWHITE'], 2) : null,
                    'asian' =>      isset($row[$i.'COUNTYAI']) ? round($row[$i.'COUNTYAI'], 2) : null,
                    'hispanic' =>   isset($row[$i.'COUNTYHISPANIC']) ? round($row[$i.'COUNTYHISPANIC'], 2) : null,
                    'other' =>      isset($row[$i.'COUNTYOTHER']) ? round($row[$i.'COUNTYOTHER'], 2) : null,
                    'otherinfo' =>  $otherinfo
                );
            }
        }

        // Return the array
        return $county_info;
    }

    /**
     * Generate some generic information for a district, including voting and demographic statistics, images
     * @param string $district
     * @param array $row
     * @param array $dist_rows
     * @param string $distColName
     * @return array
     */
    private function create_general_district_info($district, $row, $distColName){

        // Return the generic information
        return array(
            'districtTitle' => $this->getDistrictTitle($district, $row),
            'type' => $district,
            'cvb' => isset($row['CBV']) ? $row['CBV'] : '',
            'registered' => array(
                'dem' => isset($row['RegisteredDEM']) ? $row['RegisteredDEM'] : null,
                'rep' => isset($row['RegisteredREP']) ? $row['RegisteredREP'] : null,
                'una' => isset($row['RegisteredUNA']) ? $row['RegisteredUNA'] : null,
                'lib' => isset($row['RegisteredLIB']) ? $row['RegisteredLIB'] : null
            ),
            'voters' => array(
                'black' => isset($row['RegisteredBlack']) ? $row['RegisteredBlack'] : null,
                'white' => isset($row['RegisteredWhite']) ? $row['RegisteredWhite'] : null,
                'asian' => isset($row['RegisteredAI']) ? $row['RegisteredAI'] : null,
                'hispanic' => isset($row['RegisteredHispanic']) ? $row['RegisteredHispanic'] : null,
                'other' => isset($row['RegisteredOther']) ? $row['RegisteredOther'] : null
            ),
            'gender' => array(
                'm' => isset($row['RegisteredMale']) ? $row['RegisteredMale'] : null,
                'f' => isset($row['RegisteredFemale']) ? $row['RegisteredFemale'] : null
            ),
            'smallmap' => $this->generate_smallmap($district, $row, $distColName),
            'largemap' => $this->generate_largemap($district, $row, $distColName)
        );
    }

    /**
     * Generate a small map entry
     * @param string $district
     * @param array $row
     * @param string $distColName
     * @return array
     */
    private function generate_smallmap($district, $row, $distColName){
        // First, we can generalize the 'smallmap' feature
        $smallmap = array();

        // Depending on the district, smallmap means different things
        switch($district){
            case 'judicial':
                $smallmap = null;
                break;
            case 'council':
            case 'ussenate':
                $smallmap = array(
                    'background' => 'url(' . $this->frontUrl . 'images/maps/nc.jpg) no-repeat center',
                    'background-size' => 'cover'
                );
                break;
            case 'house':
            case 'senate':
            case 'congress':
                $smallmap = array(
                    'background' => 'url('.$this->frontUrl . 'images/maps/'.$district.'_340/map_'.$row[$distColName].'.jpg) no-repeat center',
                    'background-size' => 'cover'
                );
                break;

        }

        return $smallmap;
    }

    /**
     * Generate a large map
     * @param string $district
     * @param array $row
     * @param string $distColName
     * @return array
     */
    private function generate_largemap($district, $row, $distColName){
        // First, we can generalize the 'largemap' feature
        $largemap = array();

        // Depending on the district, largemap means different things
        switch($district){
            case 'judicial':
                $largemap = null;
                break;
            case 'council':
                continue;
            case 'ussenate':
                $largemap = $this->frontUrl . 'images/maps/nc.jpg';
                break;
            case 'house':
            case 'senate':
            case 'congress':
                $largemap = $this->frontUrl . 'images/maps/'.$district.'_1000/map_'.$row[$distColName].'.jpg';
        }

        return $largemap;
    }

    

    function us_senate_canddata($candidates){
        // Candidates
        if($this->db->table_exists('general_election_data_ussenate')) {
            $query = $this->db->query("SELECT * FROM `general_election_data_ussenate`");
            $rows = $query->result_array();
            foreach($rows as $i=>$row){
                $candNum = 1;
                while($candNum <= $this->max_cands){
                    if(isset($row['Candidate'.$candNum.'Name']) && $row['Candidate'.$candNum.'Name'] != ''){
                        $candidates[$row['Candidate'.$candNum.'Name']] = array(
                            'uniqueID' => 'ussenate_'.$candNum,
                            'name' => isset($row['Candidate'.$candNum.'Name']) ? $row['Candidate'.$candNum.'Name'] : null,
                            'appid' => isset($row['Candidate'.$candNum.'APPID']) ? $row['Candidate'.$candNum.'APPID'] : null,
                            'image' => isset($row['Candidate'.$candNum.'ImageUrl']) ? $this->frontUrl . $row['Candidate'.$candNum.'ImageUrl'] : null,
                            'smallimage' => isset($row['Candidate'.$candNum.'SmallImageUrl']) ? $this->frontUrl . $row['Candidate'.$candNum.'SmallImageUrl'] : null,
                            'profile' => $this->generate_style_array($row, $candNum),
                            'small_profile' => $this->generate_small_style_array($row, $candNum),
                            'county' => isset($row['Candidate'.$candNum.'County']) ? $row['Candidate'.$candNum.'County'] : null,
                            'type' => 'ussenate',
                            'aff' => isset($row['Candidate'.$candNum.'Party']) ? $row['Candidate'.$candNum.'Party'] : null,
                            'range' => $this->setlnRange($row['Candidate'.$candNum.'Name']),
                            'incumbent' => isset($row['Candidate'.$candNum.'Incumbent']) ? ($row['Candidate'.$candNum.'Incumbent'] == 'Incumbent' ? 1 : 0) : 0,
                            'occupation' => isset($row['Candidate'.$candNum.'Occupation']) ? $row['Candidate'.$candNum.'Occupation']: null,
                            'employer' => isset($row['Candidate'.$candNum.'Employer']) ? $row['Candidate'.$candNum.'Employer'] : null,
                            'prevoffice' => (isset($row['Candidate'.$candNum.'PreviousOffice']) &&  $row['Candidate'.$candNum.'PreviousOffice'] != '') ? $this->ltrim_array(explode(';', $row['Candidate'.$candNum.'PreviousOffice'])) : null,
                            'coh' => isset($row['Candidate'.$candNum.'COH']) ? $row['Candidate'.$candNum.'COH'] : null,
                            'address1' => isset($row['Candidate'.$candNum.'MailingAddress']) ? $row['Candidate'.$candNum.'MailingAddress'] : null,
                            'address2' => isset($row['Candidate'.$candNum.'CityStateZip']) ? $row['Candidate'.$candNum.'CityStateZip'] : null,
                            'phone' => isset($row['Candidate'.$candNum.'Phone']) ? $row['Candidate'.$candNum.'Phone'] : null,
                            'fax' => isset($row['Candidate'.$candNum.'Fax']) ? $row['Candidate'.$candNum.'Fax'] : null,
                            'email' => isset($row['Candidate'.$candNum.'Email']) ? $row['Candidate'.$candNum.'Email'] : null,
                            'website' => isset($row['Candidate'.$candNum.'Website']) ? $row['Candidate'.$candNum.'Website'] : null,
                            'facebook' => isset($row['Candidate'.$candNum.'Facebook']) ? $row['Candidate'.$candNum.'Facebook'] : null,
                            'twitter' => isset($row['Candidate'.$candNum.'Twitter']) ? $row['Candidate'.$candNum.'Twitter'] : null,
                            'twittername' => isset($row['Candidate'.$candNum.'Twitter']) ? $this->twittername($row['Candidate'.$candNum.'Twitter']) : null,
                            'questionaire' => isset($row['Candidate'.$candNum.'Questionaire']) ? $row['Candidate'.$candNum.'Questionaire'] : null,
                            'lbr' => isset($row['Candidate'.$candNum.'LBRScore']) ? round($row['Candidate'.$candNum.'LBRScore'], 1) : null
                        );
                    }
                    $candNum++;
                }
            }
        }
        return $candidates;
    }

    function council_cand_data($candidates){
        // Candidates
        if($this->db->table_exists('general_election_data_council')) {
            $query = $this->db->query("SELECT * FROM `general_election_data_council`");
            $rows = $query->result_array();
            foreach($rows as $i=>$row){
                $candNum = 1;
                while($candNum <= $this->max_cands){
                    if(isset($row['Candidate'.$candNum.'Name']) && $row['Candidate'.$candNum.'Name'] != ''){
                        $candidates[$row['Candidate'.$candNum.'Name']] = array(
                            'uniqueID' => 'ussenate_'.$candNum,
                            'name' => isset($row['Candidate'.$candNum.'Name']) ? $row['Candidate'.$candNum.'Name'] : null,
                            'appid' => isset($row['Candidate'.$candNum.'APPID']) ? $row['Candidate'.$candNum.'APPID'] : null,
                            'image' => isset($row['Candidate'.$candNum.'ImageUrl']) ? $this->frontUrl . $row['Candidate'.$candNum.'ImageUrl'] : null,
                            'smallimage' => isset($row['Candidate'.$candNum.'SmallImageUrl']) ? $this->frontUrl . $row['Candidate'.$candNum.'SmallImageUrl'] : null,
                            'profile' => $this->generate_style_array($row, $candNum),
                            'small_profile' => $this->generate_small_style_array($row, $candNum),
                            'county' => isset($row['Candidate'.$candNum.'County']) ? $row['Candidate'.$candNum.'County'] : null,
                            'type' => 'council',
                            'aff' => isset($row['Candidate'.$candNum.'Party']) ? $row['Candidate'.$candNum.'Party'] : null,
                            'range' => $this->setlnRange($row['Candidate'.$candNum.'Name']),
                            'incumbent' => isset($row['Candidate'.$candNum.'Incumbent']) ? ($row['Candidate'.$candNum.'Incumbent'] == 'Incumbent' ? 1 : 0) : 0,
                            'occupation' => isset($row['Candidate'.$candNum.'Occupation']) ? $row['Candidate'.$candNum.'Occupation']: null,
                            'employer' => isset($row['Candidate'.$candNum.'Employer']) ? $row['Candidate'.$candNum.'Employer'] : null,
                            'prevoffice' => (isset($row['Candidate'.$candNum.'PreviousOffice']) &&  $row['Candidate'.$candNum.'PreviousOffice'] != '') ? $this->ltrim_array(explode(';', $row['Candidate'.$candNum.'PreviousOffice'])) : null,
                            'coh' => isset($row['Candidate'.$candNum.'COH']) ? $row['Candidate'.$candNum.'COH'] : null,
                            'address1' => isset($row['Candidate'.$candNum.'MailingAddress']) ? $row['Candidate'.$candNum.'MailingAddress'] : null,
                            'address2' => isset($row['Candidate'.$candNum.'CityStateZip']) ? $row['Candidate'.$candNum.'CityStateZip'] : null,
                            'phone' => isset($row['Candidate'.$candNum.'Phone']) ? $row['Candidate'.$candNum.'Phone'] : null,
                            'fax' => isset($row['Candidate'.$candNum.'Fax']) ? $row['Candidate'.$candNum.'Fax'] : null,
                            'email' => isset($row['Candidate'.$candNum.'Email']) ? $row['Candidate'.$candNum.'Email'] : null,
                            'website' => isset($row['Candidate'.$candNum.'Website']) ? $row['Candidate'.$candNum.'Website'] : null,
                            'facebook' => isset($row['Candidate'.$candNum.'Facebook']) ? $row['Candidate'.$candNum.'Facebook'] : null,
                            'twitter' => isset($row['Candidate'.$candNum.'Twitter']) ? $row['Candidate'.$candNum.'Twitter'] : null,
                            'twittername' => isset($row['Candidate'.$candNum.'Twitter']) ? $this->twittername($row['Candidate'.$candNum.'Twitter']) : null,
                            'questionaire' => isset($row['Candidate'.$candNum.'Questionaire']) ? $row['Candidate'.$candNum.'Questionaire'] : null,
                            'lbr' => isset($row['Candidate'.$candNum.'LBRScore']) ? round($row['Candidate'.$candNum.'LBRScore'], 1) : null,
                            'office' => $row['Office']
                        );
                    }
                    $candNum++;
                }
            }
        }
        return $candidates;
    }

    function judicial_cands($candidates){
        // Candidates
        if($this->db->table_exists('general_election_data_judicial')) {
            $query = $this->db->query("SELECT * FROM `general_election_data_judicial`");
            $rows = $query->result_array();
            foreach($rows as $i=>$row){
                $candNum = 1;
                while($candNum <= $this->max_cands){
                    if(isset($row['Candidate'.$candNum.'Name']) && $row['Candidate'.$candNum.'Name'] != ''){
                        $candidates[$row['Candidate'.$candNum.'Name']] = array(
                            'uniqueID' => 'judicial_seat'.$i.'_'.$candNum,
                            'seatnum' => $i,
                            'candNum' => $candNum,
                            'name' => isset($row['Candidate'.$candNum.'Name']) ? $row['Candidate'.$candNum.'Name'] : null,
                            'appid' => isset($row['Candidate'.$candNum.'APPID']) ? $row['Candidate'.$candNum.'APPID'] : null,
                            'image' => isset($row['Candidate'.$candNum.'ImageUrl']) ? $this->frontUrl . $row['Candidate'.$candNum.'ImageUrl'] : null,
                            'smallimage' => isset($row['Candidate'.$candNum.'SmallImageUrl']) ? $this->frontUrl . $row['Candidate'.$candNum.'SmallImageUrl'] : null,
                            'profile' => $this->generate_style_array($row, $candNum),
                            'small_profile' => $this->generate_small_style_array($row, $candNum),
                            'county' => isset($row['Candidate'.$candNum.'County']) ? $row['Candidate'.$candNum.'County'] : null,
                            'seat' => isset($row['JudicialSeat']) ? $row['JudicialSeat'] : null,
                            'type' => 'judicial',
                            'aff' => isset($row['Candidate'.$candNum.'Party']) ? $row['Candidate'.$candNum.'Party'] : null,
                            'range' => $this->setlnRange($row['Candidate'.$candNum.'Name']),
                            'incumbent' => isset($row['Candidate'.$candNum.'Incumbent']) ? ($row['Candidate'.$candNum.'Incumbent'] == 'Incumbent' ? 1 : 0) : 0,
                            'occupation' => isset($row['Candidate'.$candNum.'Occupation']) ? $row['Candidate'.$candNum.'Occupation']: null,
                            'employer' => isset($row['Candidate'.$candNum.'Employer']) ? $row['Candidate'.$candNum.'Employer'] : null,
                            'prevoffice' => (isset($row['Candidate'.$candNum.'PreviousOffice']) &&  $row['Candidate'.$candNum.'PreviousOffice'] != '') ? $this->ltrim_array(explode(';', $row['Candidate'.$candNum.'PreviousOffice'])) : null,
                            'coh' => isset($row['Candidate'.$candNum.'COH']) ? $row['Candidate'.$candNum.'COH'] : null,
                            'address1' => isset($row['Candidate'.$candNum.'MailingAddress']) ? $row['Candidate'.$candNum.'MailingAddress'] : null,
                            'address2' => isset($row['Candidate'.$candNum.'CityStateZip']) ? $row['Candidate'.$candNum.'CityStateZip'] : null,
                            'phone' => isset($row['Candidate'.$candNum.'Phone']) ? $row['Candidate'.$candNum.'Phone'] : null,
                            'fax' => isset($row['Candidate'.$candNum.'Fax']) ? $row['Candidate'.$candNum.'Fax'] : null,
                            'email' => isset($row['Candidate'.$candNum.'Email']) ? $row['Candidate'.$candNum.'Email'] : null,
                            'website' => isset($row['Candidate'.$candNum.'Website']) ? $row['Candidate'.$candNum.'Website'] : null,
                            'facebook' => isset($row['Candidate'.$candNum.'Facebook']) ? $row['Candidate'.$candNum.'Facebook'] : null,
                            'twitter' => isset($row['Candidate'.$candNum.'Twitter']) ? $row['Candidate'.$candNum.'Twitter'] : null,
                            'twittername' => isset($row['Candidate'.$candNum.'Twitter']) ? $this->twittername($row['Candidate'.$candNum.'Twitter']) : null,
                            'questionaire' => isset($row['Candidate'.$candNum.'Questionaire']) ? $row['Candidate'.$candNum.'Questionaire'] : null
                        );
                    }
                    $candNum++;
                }
            }
        }
        return $candidates;
    }

    function menu(){
        $return = array();
        // Congressional Districts
        if($this->db->table_exists('general_election_data_congress')) {
            $query = $this->db->query("SELECT CD as District FROM `general_election_data_congress`");
            $return['districts_congress'] = $query->result_array();
        }
        // NC House Districts
        if($this->db->table_exists('general_election_data_house')) {
            $query = $this->db->query("SELECT NCHD as District FROM `general_election_data_house`");
            $return['districts_nchouse'] = $query->result_array();
        }
        // NC Senate Districts
        if($this->db->table_exists('general_election_data_senate')) {
            $query = $this->db->query("SELECT SD as District FROM `general_election_data_senate`");
            $return['districts_ncsenate'] = $query->result_array();
        }
        // US Senate Districts
        if($this->db->table_exists('general_election_data_ussenate')) {
            $query = $this->db->query("SELECT * FROM `general_election_data_ussenate`");
            $return['districts_ussenate'] = $query->result_array();
        }
        // US Senate Districts
        if($this->db->table_exists('general_election_data_council')) {
            $query = $this->db->query("SELECT `Office` FROM `general_election_data_council`");
            $return['districts_council'] = $query->result_array();
        }
        // NC Judicial Seats
        if($this->db->table_exists('general_election_data_judicial')) {
            $query = $this->db->query("SELECT JudicialSeat FROM `general_election_data_judicial`");
            $results = $query->result_array();
            foreach($results as $i=>$v){
                $results[$i]['id'] = $i;
            }
            $return['judicial'] = $results;
        }

        // Pages
        $return['pages'] = $this->db->query("SELECT title, id FROM pages WHERE published='1'")->result();

        // Directory listings by table
        $directories = array('cabinet', 'house', 'senate', 'congress');
        foreach($directories as $i=>$directory){
            if($this->db->table_exists('directory_'.$directory)) {
                $directories[$i] = array('name' => ucfirst($directory), 'urlstring' => 'directory_'.$directory);
            } else {
                unset($directories[$i]);
            }
        }
        $return['directories'] = $directories;
        return $return;
    }

    function candidates(){
        // Candidates
        $candidates = array();
        $houses = array('congress', 'house', 'senate');
        foreach($houses as $i=>$house){
            if($this->db->table_exists('general_election_data_'.$house)) {
                $distColName = ($house == 'congress') ? 'CD' : ($house == 'house' ? 'NCHD' : 'SD');
                $query = $this->db->query("SELECT * FROM `general_election_data_$house`");
                $rows = $query->result_array();
                foreach($rows as $i=>$row){
                    $candNum = 1;
                    while($candNum <= $this->max_cands){
                        if(isset($row['Candidate'.$candNum.'Name']) && $row['Candidate'.$candNum.'Name'] != ''){
                            $candidates[$row['Candidate'.$candNum.'Name']] = array(
                                'uniqueID' => $house.'_'.$row[$distColName].'_'.$candNum,
                                'name' => isset($row['Candidate'.$candNum.'Name']) ? $row['Candidate'.$candNum.'Name'] : null,
                                'appid' => isset($row['Candidate'.$candNum.'APPID']) ? $row['Candidate'.$candNum.'APPID'] : null,
                                'image' => isset($row['Candidate'.$candNum.'ImageUrl']) ? $this->frontUrl . $row['Candidate'.$candNum.'ImageUrl'] : null,
                                'smallimage' => isset($row['Candidate'.$candNum.'SmallImageUrl']) ? $this->frontUrl . $row['Candidate'.$candNum.'SmallImageUrl'] : null,
                                'profile' => $this->generate_style_array($row, $candNum),
                                'small_profile' => $this->generate_small_style_array($row, $candNum),
                                'county' => isset($row['Candidate'.$candNum.'County']) ? $row['Candidate'.$candNum.'County'] : null,
                                'type' => $house,
                                'aff' => isset($row['Candidate'.$candNum.'Party']) ? $row['Candidate'.$candNum.'Party'] : null,
                                'district' => $row[$distColName],
                                'range' => $this->setlnRange($row['Candidate'.$candNum.'Name']),
                                'incumbent' => isset($row['Candidate'.$candNum.'Incumbent']) ? ($row['Candidate'.$candNum.'Incumbent'] == 'Incumbent' ? 1 : 0) : 0,
                                'occupation' => isset($row['Candidate'.$candNum.'Occupation']) ? $row['Candidate'.$candNum.'Occupation']: null,
                                'employer' => isset($row['Candidate'.$candNum.'Employer']) ? $row['Candidate'.$candNum.'Employer'] : null,
                                'prevoffice' => (isset($row['Candidate'.$candNum.'PreviousOffice']) &&  $row['Candidate'.$candNum.'PreviousOffice'] != '') ? $this->ltrim_array(explode(';', $row['Candidate'.$candNum.'PreviousOffice'])) : null,
                                'coh' => isset($row['Candidate'.$candNum.'COH']) ? $row['Candidate'.$candNum.'COH'] : null,
                                'address1' => isset($row['Candidate'.$candNum.'MailingAddress']) ? $row['Candidate'.$candNum.'MailingAddress'] : null,
                                'address2' => isset($row['Candidate'.$candNum.'CityStateZip']) ? $row['Candidate'.$candNum.'CityStateZip'] : null,
                                'phone' => isset($row['Candidate'.$candNum.'Phone']) ? $row['Candidate'.$candNum.'Phone'] : null,
                                'fax' => isset($row['Candidate'.$candNum.'Fax']) ? $row['Candidate'.$candNum.'Fax'] : null,
                                'email' => isset($row['Candidate'.$candNum.'Email']) ? $row['Candidate'.$candNum.'Email'] : null,
                                'website' => isset($row['Candidate'.$candNum.'Website']) ? $row['Candidate'.$candNum.'Website'] : null,
                                'facebook' => isset($row['Candidate'.$candNum.'Facebook']) ? $row['Candidate'.$candNum.'Facebook'] : null,
                                'twitter' => isset($row['Candidate'.$candNum.'Twitter']) ? $row['Candidate'.$candNum.'Twitter'] : null,
                                'twittername' => isset($row['Candidate'.$candNum.'Twitter']) ? $this->twittername($row['Candidate'.$candNum.'Twitter']) : null,
                                'questionaire' => isset($row['Candidate'.$candNum.'Questionaire']) ? $row['Candidate'.$candNum.'Questionaire'] : null,
                                'lbr' => isset($row['Candidate'.$candNum.'LBRScore']) ? round($row['Candidate'.$candNum.'LBRScore'], 1) : null
                            );
                        }
                        $candNum++;
                    }
                }
            }
        }
        $candidates = $this->us_senate_canddata($candidates);
        $candidates = $this->judicial_cands($candidates);
        $candidates = $this->council_cand_data($candidates);
        return $candidates;

    }

    function generate_style_array($row, $candNum = false){
        if($candNum){
            $image = isset($row['Candidate'.$candNum.'ImageUrl']) ? $this->frontUrl . $row['Candidate'.$candNum.'ImageUrl'] : null;
            return array(
                'background' => 'url('.$image.') no-repeat center',
                'background-size' => 'cover'
            );
        } else {
            $image = isset($row['ImageUrl']) ? $this->frontUrl . $row['ImageUrl'] : null;
            return array(
                'background' => 'url('.$image.') no-repeat center',
                'background-size' => 'cover'
            );
        }

    }

    function generate_small_style_array($row, $candNum = false){
        if($candNum){
            $image = isset($row['Candidate'.$candNum.'SmallImageUrl']) ? $this->frontUrl . $row['Candidate'.$candNum.'SmallImageUrl'] : null;
            return array(
                'background' => 'url('.$image.') no-repeat center',
                'background-size' => 'cover'
            );
        } else {
            $image = isset($row['SmallImageUrl']) ? $this->frontUrl . $row['SmallImageUrl'] : null;
            return array(
                'background' => 'url('.$image.') no-repeat center',
                'background-size' => 'cover'
            );
        }

    }

    function generate_small_image_url($row, $candNum = false){
        if($candNum){
            return isset($row['Candidate'.$candNum.'SmallImageUrl']) ? $this->frontUrl . $row['Candidate'.$candNum.'SmallImageUrl'] : null;
        } else {
            return isset($row['SmallImageUrl']) ? $this->frontUrl . $row['SmallImageUrl'] : null;
        }
    }

    function generate_image_url($row, $candNum = false){
        if($candNum){
            return isset($row['Candidate'.$candNum.'ImageUrl']) ? $this->frontUrl . $row['Candidate'.$candNum.'ImageUrl'] : null;
        } else {
            return isset($row['SmallImageUrl']) ? $this->frontUrl . $row['ImageUrl'] : null;
        }
    }

    function pages(){
        $return = array();
        $pages = $this->db->get_where('pages', array('published' => 1))->result();
        foreach($pages as $page){
            $return[$page->id] = $page;
        }
        return $return;
    }

    

    function members_post(){
        $key = $this->post('token');
        if(strlen($key) == 32){
             $this->response($this->candidates(), 200);
         } else {
             $this->response('Not Authorized', 401);
         }
       
    }

    

    function setlnRange($fullname){
        $n = strpos($fullname,  ',');
        $fullname = substr($fullname, 0, $n === false ? strlen($fullname) : $n);
        $fullname = explode(' ', $fullname);
        $fullname = end($fullname);
        $letter = strtoupper(substr($fullname, 0, 1));
        $setRange = '';
        $ad = ['A', 'B', 'C', 'D'];
        $eh = ['E', 'F', 'G', 'H'];
        $il = ['I', 'J', 'K', 'L'];
        $mp = ['M', 'N', 'O', 'P'];
        $qt = ['Q', 'R', 'S', 'T'];
        $uz = ['U', 'V', 'W', 'X', 'Y', 'Z'];
        if(in_array($letter, $ad)){
            $setRange = 'A-D';
        } else if(in_array($letter, $eh)){
            $setRange = 'E-H';
        } else if(in_array($letter, $il)){
            $setRange = 'I-L';
        } else if(in_array($letter, $mp)){
            $setRange = 'M-P';
        } else if(in_array($letter, $qt)){
            $setRange = 'Q-T';
        } else if(in_array($letter, $uz)){
            $setRange = 'U-Z';
        }
        return $setRange;
    }

    function ltrim_array($array){
        foreach($array as $key=>$val){
            $array[$key] = ltrim($val);
        }
        return $array;
    }

    function twittername($twitterUrl){
        if(preg_match("|https?://(www\.)?twitter\.com/(#!/)?@?([^/]*)|", $twitterUrl, $matches)){
            return $matches[3];
        }
    }

    function getDistrictTitle($district, $row){
        switch($district){
            case 'congress':
                return 'US House';
                break;
            case 'senate':
                return 'NC Senate';
                break;
            case 'house':
                return 'NC House';
                break;
            case 'judicial':
                return $row['JudicialSeat'];
                break;
            case 'ussenate':
                return 'US Senate';
                break;
            case 'council':
                return $row['Office'];
                break;
        }
    }


}