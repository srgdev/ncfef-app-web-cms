'use strict';

/* Services */

almanac.service('Data', function(){

    var districts;
    var candidates;
    var candidatesArray;
    var directory;
    var directoryListings = [];

    candidates = $.map(almanacData.candidates, function(value, index) {
        return [value];
    });
    candidatesArray = candidates;
    candidates = TAFFY(candidates);

    districts = [];
    $.each(almanacData.districts, function(i, district){
        if(i != 'ussenate'){
            $.map(district, function(value, index) {
                districts.push(value);
            });

        }

    });
    districts = TAFFY(districts);

    directory = [];
    $.each(almanacData.directory, function(i, dir){
        directory[i] = [];
        $.map(dir, function(value, index) {
            if(typeof(value.CommitteeAssignments) === 'string' && value.CommitteeAssignments != ''){
                var comms = value.CommitteeAssignments.split(';')
                var objarr = [];
                angular.forEach(comms, function(val, index){
                    if(val != '' && val != ' ' && val.length > 2){
                        objarr.push({name: val});
                    }
                })
                value.CommitteeAssignments = objarr;
            }
            if(typeof(value.LegislativeAssistants) === 'string' && value.LegislativeAssistants != ''){
                var comms = value.LegislativeAssistants.split(';')
                var objarr = [];
                angular.forEach(comms, function(val, index){
                    if(val != '' && val != ' ' && val.length > 2){
                        objarr.push({name: val});
                    }
                })
                value.LegislativeAssistants = objarr;
            }
            directory[i].push(value);
        });
        directory[i] = TAFFY(directory[i]);
    });

    $.map(almanacData.directory.cabinet, function(value, index) {
        value.type= 'cabinet';
        value.id = index;
        directoryListings.push(value);
    });

    $.map(almanacData.directory.house, function(value, index) {
        value.type = 'house';
        value.id = index;
        directoryListings.push(value);
    });

    $.map(almanacData.directory.senate, function(value, index) {
        value.type = 'senate';
        value.id = index;
        directoryListings.push(value);
    });

    $.map(almanacData.directory.us_congress, function(value, index) {
        value.type = 'us_congress';
        value.id = index;
        directoryListings.push(value);
    });

    if(typeof almanacData.directory.ussenate != 'undefined' ){
        $.map(almanacData.directory.ussenate, function(value, index) {
            value.type = 'ussenate';
            value.id = index;
            directoryListings.push(value);
        });    
    }

    if(typeof almanacData.directory.judicial != 'undefined' ){
        $.map(almanacData.directory.judicial, function(value, index) {
            value.type = 'judicial';
            value.id = index;
            directoryListings.push(value);
        });    
    }
    

    return {
        rawdata: almanacData,
        districts: districts,
        candidates: candidates,
        candidatesArray: candidatesArray,
        ussenate: almanacData.districts.ussenate,
        menu: almanacData.menu,
        directory: directory,
        directoryListings: directoryListings,
    }
})

almanac.service('Linking', function($state){
    return {
        getDistrictLink: function(type, district){
            if(district == 'Statewide'){
                return $state.href("ussenate");
            } else {
                return $state.href("district", { districttype: type, district: district});
            }
        },
        getDistrictListLink: function(type, district){
            if(district == 'Statewide'){
                return $state.href("ussenate");
            } else {
                if(type == 'ussenate'){
                return $state.href("ussenate");
            } else {
                return $state.href("candidateListDistrict", { districttype: type, district: district});
            }    
            }
            
        },
        getDirLink: function(type, id){
            if(type == 'cabinet'){
                return $state.href("cabinet", { dirId: id });
            } else {
                return $state.href("directory", { dirType: type, dirId: id });
            }
        },
        getCandidateLink: function(appid, districttype){
            return $state.href("candidate", { appid: appid, districttype: districttype });
        },
        getJudicialLink: function(id){
            return $state.href("judicialseat", {id: id});
        },
        getCongressLink: function(district){
            return $state.href('district', {districttype: 'congress', district: district});
        },
        getHouseLink: function(district){
            return $state.href('district', {districttype: 'house', district: district});
        },
        getSenateLink: function(district){
            return $state.href('district', {districttype: 'senate', district: district});
        },
        getCouncilLink: function(office){
            return $state.href('council', {office: office});
        }
    }
})

almanac.service('Interface', function(){
    return {
        init: function(){

            $(".initialLoad").fadeOut();

            $('.dist .profile').click(function(e){
                var full = $('.profileFull');
                if(full[0]){
                    var fullWidth = full.outerWidth();
                    var windowWidth = $('#wrapper').outerWidth();
                    var fullLeft = ((windowWidth-fullWidth)/2);
                    full.show().css('left', fullLeft+'px');
                    $(window).resize(function(){
                        var fullWidth = full.outerWidth();
                        var windowWidth = $('#wrapper').outerWidth();
                        var fullLeft = ((windowWidth-fullWidth)/2);
                        full.css('left', fullLeft+'px');
                    })
                    $('.blur').unbind().addClass('active');
                    full.click(function(e){
                        full.hide();
                        $('.blur').removeClass('active');
                    })
                    $('.blur').click(function(e){
                        full.hide();
                        $('.blur').removeClass('active');
                    })
                }

            })

            $('.swipe').scroll(function(){
                var right = $(this).scrollLeft();
                if(right > 0){
                    $(this).addClass('scrolled');
                } else {
                    $(this).removeClass('scrolled');
                }
            });

            $('.swipe').mousedown(function(){
                var that = $(this);
                $(window).mousemove(function() {
                    var style =   that.find('.panhandler-wrap').css('transform');// Need the DOM object
                    if(typeof style != 'undefined'){
                        var array = style.split('(')[1].split(')')[0].split(',');
                        if(array[4] < 0){
                            that.addClass('scrolled');
                        } else {
                            that.removeClass('scrolled');
                        }
                    }
                });
            });

            var scrollLeft;
            $('.scrollButton').mousedown(function(){
                var target = $(this).parent().find('.swipe');
                scrollLeft = setInterval(function(){
                    target.animate({scrollLeft: '+=40'}, 200, "linear");
                }, 200);
            }).mouseup(function(){
                clearInterval(scrollLeft);
            });

            var scrollRight;
            $('.scrollButton.left').unbind('mousedown').mousedown(function(){
                var target = $(this).parent().find('.swipe');
                scrollRight = setInterval(function(){
                    target.animate({scrollLeft: '-=40'}, 200, "linear");
                }, 200);
            }).mouseup(function(){
                clearInterval(scrollRight);
            });

            if($('.mainMenu').hasClass('active')){
                $('.mainMenu, .blur').toggleClass('active');
            }

//            $(document).on('touchmove',function(e){
//                e.preventDefault();
//            });

            //uses body because jquery on events are called off of the element they are
            //added to, so bubbling would not work if we used document instead.
            $('body').on('touchstart','.scrollable',function(e) {
                if (e.currentTarget.scrollTop === 0) {
                    e.currentTarget.scrollTop = 1;
                } else if (e.currentTarget.scrollHeight === e.currentTarget.scrollTop + e.currentTarget.offsetHeight) {
                    e.currentTarget.scrollTop -= 1;
                }
            });

            //prevents preventDefault from being called on document if it sees a scrollable div
//            $('body').on('touchmove','.scrollable',function(e) {
//                var totalHeight = 0;
//                $(this).children().each(function() {
//                    if(!$(this).hasClass('blur')){
//                        totalHeight += $(this).outerHeight();
//                    }
//                });
//                if(totalHeight > $(this).innerHeight()) {
//                    e.stopPropagation();
//                }
//            });

        },
        showLoader: function(){
            $(".initialLoad").fadeIn();
        },
        hideLoader: function(){
            $(".initialLoad").fadeOut();
        }
    }
});