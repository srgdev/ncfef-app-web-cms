'use strict';

/* Test for IE < 9 */

var ie = (function(){
    var undef,
        v = 3,
        div = document.createElement('div'),
        all = div.getElementsByTagName('i');
    while (
        div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
            all[0]
        );
    return v > 4 ? v : undef;
}());

if(ie < 9){
    console.log('!9');
    $(document).ready(function(){
        $('.spinner').fadeOut();
        $('.text').html('<span style="color:#fff;font-family: sans-serif; font-size:11px;">This site only supports Internet Explorer 9 or newer. Please, <a style="color: #fff;display: block;text-transform: uppercase;background: #AB3132;margin-top: 10px;width: 180px;padding: 10px 0;border: 1px solid #673432" href="http://browsehappy.com/">Upgrade your browser.</a></span>');
    })
}

/* App Module */
var almanacData;

angular.element(document).ready(function() {
    if(window.addEventListener){
        window.addEventListener('load', function() {
            FastClick.attach(document.body);
        }, false);
    } else {
        window.attachEvent('load', function() {
            FastClick.attach(document.body);
        });
    }

    if($.isEmptyObject(amplify.store('almanacdata')) || $.isEmptyObject(amplify.store('almanaclastupdate'))){
        console.log('No app data present, loading data...');
        jQuery.getJSON(urlBase + '/api/appdata?callback=', function(data) {
            amplify.store('almanacdata', data);
            console.log(data);
            console.log('Data loaded');
            almanacData = data;
            jQuery.getJSON(urlBase + '/api/lastupdate?callback=', function(data){
                amplify.store('almanaclastupdate', data);
                console.log('Update date set to ' + data.lastupdate);
            })
            angular.bootstrap(document.getElementById('almanac'), ['almanac']);
        });
    } else {
        console.log('Data found, checking for updates...');
        jQuery.getJSON(urlBase + '/api/lastupdate?callback=', function(data){
            var userlastupdated = amplify.store('almanaclastupdate');
            if(Number(data.lastupdate) > Number(userlastupdated.lastupdate)){
                console.log('Update available, updating...');
                jQuery.getJSON(urlBase + '/api/appdata?callback=', function(data) {
                    amplify.store('almanacdata', data);
                    almanacData = data;
                    console.log('Data loaded');
                    jQuery.getJSON(urlBase + '/api/lastupdate?callback=', function(data){
                        amplify.store('almanaclastupdate', data);
                        console.log('Update date set to ' + data.lastupdate);
                    })
                    angular.bootstrap(document.getElementById('almanac'), ['almanac']);
                });
            } else {
                console.log('No update available, starting...');
                almanacData = amplify.store('almanacdata');
                angular.bootstrap(document.getElementById('almanac'), ['almanac']);
            }
        })

    }
});



var almanac = angular.module('almanac', ['ui.router', 'ngAnimate', 'ngTouch', 'panhandler'])

almanac.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/home');

    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: 'partials/home.html',
            controller: 'home'
        })
        .state('candidates', {
            url: '/candidates',
            templateUrl: 'partials/listCandsMain.html',
            controller: 'candidateList'
        })
        .state('candidate', {
            url: '/candidate/:districttype/:appid',
            templateUrl: 'partials/candidate.html',
            controller: 'candidate'
        })
        .state('candidateListDistrict', {
            url: '/candidates/:districttype/:district',
            templateUrl: 'partials/listDistrictCands.html',
            controller: 'candidateListDistrict'
        })
        .state('districts', {
            url: '/districts/:districttype',
            templateUrl: 'partials/listDistricts.html',
            controller: 'districtList'
        })
        .state('district', {
            url: '/district/:districttype/:district',
            templateUrl: 'partials/district.html',
            controller: 'district'
        })
        .state('contact', {
            url: '/contact',
            templateUrl: 'partials/contact.html',
            controller: 'about'
        })
        .state('about', {
            url: '/about',
            templateUrl: 'partials/about.html',
            controller: 'about'
        })
        .state('directories', {
            url: '/directories',
            templateUrl: 'partials/listDirectoryMain.html',
            controller: 'directories'
        })
        .state('directory', {
            url: '/directory/:dirType/:dirId',
            templateUrl: 'partials/directory.html',
            controller: 'directory'
        })
        .state('cabinet', {
            url: '/cabinet/:dirId',
            templateUrl: 'partials/cabinet.html',
            controller: 'cabinet'
        })
        .state('ussenate', {
            url: '/ussenate',
            templateUrl: 'partials/ussenate.html',
            controller: 'ussenate'
        })
        .state('judicialseat', {
            url: '/judicialseat/:id',
            templateUrl: 'partials/judicialSeat.html',
            controller: 'judicialseat'
        })
        .state('search', {
            url: '/search',
            templateUrl: 'partials/search.html',
            controller: 'search'
        })
        .state('council', {
            url: '/council/:office',
            templateUrl: 'partials/council.html',
            controller: 'council'
        })
        .state('page', {
            url: '/page/:id',
            templateUrl: 'partials/page.html',
            controller: 'page'
        });
});

almanac.run(function($rootScope, Interface){
    $rootScope.$on('$viewContentLoaded', function(){
            $('body').scrollTop(0);
            Interface.init();
    });
})