<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Admin
 * Loads admin pages, handles user access to admin panel
 */
class Files extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function download_images(){

        ini_set('memory_limit','128M');

        $this->load->library('zip');
        $path = '../images/';
        $this->zip->read_dir($path);
        $this->zip->download('images.zip');
    }


}