<?php
/**
 * Displays an upload form that takes action at admin/upload_spreadsheet
 * Calls a function to check if spreadsheet is filetype
 * Prints errors on return false (form error handler)
 */
?>
<?php if(isset($error)): ?>
    <div class="alert alert-danger"><?php echo $error; ?></div>
<?php endif; ?>
<?php if(isset($success)): ?>
    <div class="alert alert-success"><?php echo $success; ?></div>
<?php endif; ?>
<?php echo form_open_multipart('admin/upload');?>
    <div class="form-group">
        <input type="file" name="userfile" size="20" />
    </div>
    <div class="form-group">
        <input type="hidden" name="uploading" value="true" />
        <input class="btn btn-md btn-primary" type="submit" value="upload" />
    </div>
</form>
