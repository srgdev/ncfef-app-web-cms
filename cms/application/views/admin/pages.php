<?php
/**
 * Page for editing custom app pages
 */
?>

<h2><small>App Custom Pages</small></h2>
    <table class="table">
        <?php if(isset($pages) && !empty($pages)): ?>
            <thead>
            <tr>
                <td>Title</td>
                <td>Date Updated</td>
            </tr>
            </thead>
            <tbody>
            <?php foreach($pages as $page): ?>
                <?php $time = strtotime($page->date); ?>
                <?php $date = date("Y-m-d H:i:s", $time); ?>
                <tr>
                    <td><a href="<?php echo base_url('index.php/admin/edit_page').'/'.$page->id; ?>"><?php echo $page->title; ?></a></td>
                    <td><?php echo $date; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        <?php else: ?>
            <tbody>
                <tr>
                    <td>No pages exist yet.</td>
                </tr>
            </tbody>
        <?php endif; ?>
        <tfoot>
            <tr>
                <td colspan="2"><a class="btn btn-sm btn-primary" href="<?php echo base_url('index.php/admin/add_page'); ?>">Create Page</a></td>
            </tr>
        </tfoot>
    </table>