'use strict';

/* Controllers */

almanac.controller('candidateList', ['$scope', '$rootScope', '$state', 'Data', 'Linking', function($scope, $rootScope, $state, Data, Linking){
    // Default filters
    $scope.filters = {type: 'ussenate', range: null};
    // Get candidates
    $scope.candidates = Data.candidatesArray;

    // Hook up linking
    $scope.linking = Linking;

    // Filter mod functions
    $scope.clearType = function(){
        delete $scope.filters.type;
    }
    $scope.clearName = function(){
        delete $scope.filters.range;
    }
    $scope.filterCands = function(type){
        $scope.filters.type = type;
    }

    $scope.filterRange = function(range){
        $scope.filters.range = range;
    }

    $scope.clearName();

}]);