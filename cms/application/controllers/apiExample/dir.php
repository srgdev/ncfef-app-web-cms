<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API
 */

require APPPATH.'/libraries/REST_Controller.php';

class API extends REST_Controller {

    /**
     * Responds to GET requests under /dot/
     */
    function directory_get() {
        if($this->get('dept')){
            $dept = $this->get('dept');
            $query = $this->db->query("SELECT * FROM `directory_$dept`");
            $res = $query->result_array();
            if($res) {
                $this->response($res, 200); // 200 being the HTTP response code
            } else {
                $this->response(array('error' => 'Directory listing for DOT could not be found'), 404);
            }
        } else {
            $this->response(array('error' => 'Directory listing for DOT could not be found'), 404);
        }

    }


}