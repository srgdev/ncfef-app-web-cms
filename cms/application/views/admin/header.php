<?php
/**
 * The admin header file, loads all css and handles titles
 * For admin pages
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
<!--    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">-->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/bootstrap.min.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/bootstrap-theme.min.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/layout.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('js/external/google-code-prettify/prettify.css'); ?>">

    <title><?php echo $title; ?></title>
</head>
<body>

<div class="content-header bg-primary">
    <a id="menu-toggle" href="#" class="btn btn-primary"><i class="fa fa-bars"></i></a>
    <h3><?php echo $title; ?></h3>
    <div id="logout">
        <i class="loggedin">logged in as <?php echo $user['username']; ?> </i><a href="<?php echo base_url('index.php/admin/logout'); ?>"><button class="btn btn-sm btn-default">logout</button></a>
    </div>
</div>

<div id="wrapper">

    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li>
                <a <?php echo base_url('admin') == current_url() ? 'class="active"' : ''; ?> href="<?php echo base_url('index.php/admin'); ?>">Dashboard</a>
            </li>
            <li>
                <a <?php echo base_url('admin/upload') == current_url() ? 'class="active"' : ''; ?> href="<?php echo base_url('index.php/admin/upload'); ?>">Upload Spreadsheets</a>
            </li>
            <li>
                <a <?php echo base_url('admin/manage_uploads') == current_url() ? 'class="active"' : ''; ?> href="<?php echo base_url('index.php/admin/manage_uploads'); ?>">Manage Spreadsheets</a>
            </li>
            <li>
                <a <?php echo base_url('admin/import_spreadsheets') == current_url() ? 'class="active"' : ''; ?> href="<?php echo base_url('index.php/admin/import_spreadsheets'); ?>">Import Spreadsheets to Database</a>
            </li>
            <li>
                <a <?php echo base_url('admin/upload_images') == current_url() ? 'class="active"' : ''; ?> href="<?php echo base_url('index.php/admin/upload_images'); ?>">Upload Photos</a>
            </li>
            <li>
                <a <?php echo base_url('admin/pages') == current_url() ? 'class="active"' : ''; ?> href="<?php echo base_url('index.php/admin/pages'); ?>">Pages</a>
            </li>
        </ul>
    </div>

    <!-- Page content -->
    <div id="page-content-wrapper">
        <div class="page-content inset">