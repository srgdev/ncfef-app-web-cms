'use strict';

/* Controllers */

almanac.controller('directory', ['$scope', '$rootScope', '$state', '$stateParams', 'Data', 'Linking', function($scope, $rootScope, $state, $stateParams, Data, Linking){

    $scope.listing = Data.directory[$stateParams.dirType]({id: parseInt($stateParams.dirId)}).first();
    $scope.listing.type = $stateParams.dirType;
    $scope.linking = Linking;

    $scope.cityStateAdd = function(){
        if($scope.listing.PreferredCity && $scope.listing.PreferredState){
            return true;
        } else {
            return false;
        }
    }

}]);