<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Main
 * Loads our Angular App, and it's done
 */
class Main extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    /**
     * Load the app view
     */
    function index() {
        $this->load->view('app');
    }

}