<?php
/**
 * The admin index file - default landing page after logging in
 * For admin pages
 */
?>
<h2><small>Recent Updates</small></h2>
<?php if(isset($updates) && !empty($updates)): ?>
    <table class="table">
        <thead>
        <tr>
            <td>File</td>
            <td>Table</td>
            <td>Date Updated</td>
        </tr>
        </thead>
        <tbody>
            <?php foreach($updates as $update): ?>
                <?php $time = strtotime($update->date); ?>
                <?php $date = date("Y-m-d H:i:s", $time); ?>
                <tr>
                    <?php if(file_exists("uploads/" . $update->file)): ?>
                        <td><a href="<?php echo base_url('uploads'); ?>/<?php echo $update->file; ?>"><?php echo $update->file; ?></a></td>
                    <?php else: ?>
                        <td><?php echo $update->file; ?></td>
                    <?php endif; ?>
                    <td><?php echo $update->table; ?></td>
                    <td><?php echo $date; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>