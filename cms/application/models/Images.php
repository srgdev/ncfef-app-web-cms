<?php

/**
 * Images class for uploading and managing
 */
Class Images extends CI_Model
{

    /**
     * tmp sub directory prefix
     */
    private $tmp_dir_prefix = 'tmp_';

    /**
     * tmp parent directory path
     */
    private $tmp_dir_parent = './tmp_img';

    /**
     * Constructor
     */
    function __construct(){
        parent::__construct();

        $this->load->library('image_lib');
    }

    /**
     * Main class function, extracts a zip file to a tmp folder and processes images
     * @param string zip file path
     * @param string name of zip since a folder will be created with this name in the extracted directory
     * @return bool
     */ 
    public function import_zip($zip_file, $zip_name){

        // Spool up a zip archive class
        $zipArchive = new ZipArchive();

        // Attempt to open file
        $file = $zipArchive->open($zip_file);

        // If file is valid
        if($file === true){

            // Attempt to create a temporary irectory for working with files
            $tmp_dir = $this->create_tmp_dir();

            // If we got the tmp dir
            if($tmp_dir){

                // Extract
                $zipArchive->extractTo($this->tmp_dir_parent . '/' . $tmp_dir);
                $zipArchive->close();

                $subfolder_name = substr($zip_name, 0, -4);

                // Now the fun begins; create a subfolder in the temp directory, create a batch of small and large images, return the list
                $large_parsed = $this->parse_large_images($this->tmp_dir_parent . '/' . $tmp_dir);
                $small_parsed = $this->parse_small_images($this->tmp_dir_parent . '/' . $tmp_dir);

                $parsed = array('large' => $large_parsed, 'small' => $small_parsed);

                $this->directory_copy($this->tmp_dir_parent . '/' . $tmp_dir . '/resized/', '../images/photos');

                $this->destroy_tmp_dir($this->tmp_dir_parent . '/' . $tmp_dir);

                return $parsed;

            }

        } else {

            // Failure
            return false;
        }

    }

    private function get_filepath_from_array($array, $parent_branch = false){

        $return = array();

        foreach($array as $branch=>$leaf){

            if(is_array($leaf)){

                if($parent_branch){
                    $leaves =$this->get_filepath_from_array($leaf, $parent_branch .'/' . $branch);
                } else {
                    $leaves = $this->get_filepath_from_array($leaf, $branch);
                }

                foreach($leaves as $leaf){
                    array_push($return, $leaf);
                }

            } else {

                if($parent_branch){

                    $return[] = $parent_branch . '/' . $leaf;

                } else {

                    $return[] = $leaf;    
                }
                
            }

        }

        return $return;

    }

    private function parse_large_images($dir){

        $dir_map = $this->get_filepath_from_array(directory_map($dir));

        $return = array();

        if (!file_exists($dir . '/resized')) {
            mkdir($dir . '/resized', 0755, true);
        }

        foreach($dir_map as $k => $item){

            if(getimagesize($dir .'/' . $item)){

                $config['image_library'] = 'gd2';
                $config['source_image'] = $dir .'/' . $item;
                $config['new_image'] = $dir .'/resized/';
                $config['maintain_ratio'] = TRUE;
                $config['width']    = 400;
                $config['height']   = 400;

                $this->image_lib->initialize($config);

                if($this->image_lib->resize()){
                    $return['success'][$item] = 'Successfully resized to 400px';
                } else {
                    $return['error'][$item] = $this->image_lib->display_errors();
                }

                $this->image_lib->clear();

            }

        }
        return $return;
    }

    private function parse_small_images($dir){

        $dir_map = $this->get_filepath_from_array(directory_map($dir));
        $return = array();

        if (!file_exists($dir . '/resized')) {
            mkdir($dir . '/resized', 0755, true);
        }

        if (!file_exists($dir . '/resized/small')) {
            mkdir($dir . '/resized/small', 0755, true);
        }        

        foreach($dir_map as $k => $item){

            if(getimagesize($dir .'/' . $item)){

                $config['image_library'] = 'gd2';
                $config['source_image'] = $dir .'/' . $item;
                $config['new_image'] = $dir .'/resized/small/';
                $config['maintain_ratio'] = TRUE;
                $config['width']    = 98;
                $config['height']   = 98;

                $this->image_lib->initialize($config);

                if($this->image_lib->resize()){
                    $return['success'][$item] = 'Successfully resized to 98px';
                } else {
                    $return['error'][$item] = $this->image_lib->display_errors();
                }

                $this->image_lib->clear();

            }

        }
        return $return;
    }

    /**
     * Create a temporary direcory based on current time
     * @return bool
     */
    private function create_tmp_dir(){

        $time = time();

        if (!file_exists($this->tmp_dir_parent . '/' . $this->tmp_dir_prefix . $time)) {
            if(mkdir($this->tmp_dir_parent . '/' . $this->tmp_dir_prefix . $time, 0755, true)){
                return $this->tmp_dir_prefix . $time;
            } else {
                return false;
            }
        }

        return $this->tmp_dir_prefix . $time;
    }

    /**
     * Destroy the tmp dir when we are done using it
     * @param string directory to destroy (inside the /tmp_img/ folder)
     */
    private function destroy_tmp_dir($dir){

        if (is_dir($dir))
            $dir_handle = opendir($dir);

        if (!$dir_handle)
            return false;

        while($file = readdir($dir_handle)) {
            if ($file != "." && $file != "..") {
                if (!is_dir($dir."/".$file))
                    unlink($dir."/".$file);
                else
                    $this->destroy_tmp_dir($dir.'/'.$file);
            }
        }

        closedir($dir_handle);
        rmdir($dir);

        return true;
    }

    /**
     * Copy a whole Directory
     *
     * Copy a directory recrusively ( all file and directories inside it )
     *
     * @access    public
     * @param    string    path to source dir
     * @param    string    path to destination dir
     * @return    array
     */    
    private function directory_copy($srcdir, $dstdir)
    {
        //preparing the paths
        $srcdir=rtrim($srcdir,'/');
        $dstdir=rtrim($dstdir,'/');

        //creating the destination directory
        if(!is_dir($dstdir))mkdir($dstdir, 0777, true);

        //Mapping the directory
        $dir_map=directory_map($srcdir);

        foreach($dir_map as $object_key=>$object_value)
        {
            if(is_numeric($object_key))
                copy($srcdir.'/'.$object_value,$dstdir.'/'.$object_value);//This is a File not a directory
            else
                $this->directory_copy($srcdir.'/'.$object_key,$dstdir.'/'.$object_key);//this is a directory
        }
    }
}