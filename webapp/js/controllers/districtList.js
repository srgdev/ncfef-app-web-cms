'use strict';

/* District Controller */

almanac.controller('districtList', ['$scope', '$rootScope', '$state', '$stateParams', 'Data', 'Linking', function($scope, $rootScope, $state, $stateParams, Data, Linking){
    // Get district type from state url parameters
    $scope.type = $stateParams.districttype;
    // Load in raw district array from Data service
    $scope.districts = Data.rawdata.districts[$scope.type];

    // Load linking service into scope to generate links
    $scope.linking = Linking;

    if($scope.type == 'congress'){
    	$scope.ussenate = Data.rawdata.districts.ussenate;
    }
}]);