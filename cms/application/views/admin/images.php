<?php
/**
 * Displays an upload form that takes action at admin/upload_images
 * Calls a function to check if zip is filetype
 * Prints errors on return false (form error handler)
 */
?>
<?php if(isset($error)): ?>
    <div class="alert alert-danger"><?php echo $error; ?></div>
<?php endif; ?>
<?php if(isset($success)): ?>
    <div class="alert alert-success"><?php echo $success; ?></div>
<?php endif; ?>

<?php echo form_open_multipart('admin/upload_images');?>

	<label>
		Upload a .zip file with images named appropriately.  These will be resized and duplicated to fit the app's image sizes (400x400 for large, 98x98 for small).  Images MUST be in JPEG format with the extension ".jpg" in order to work.  Upload images before importing new spreadsheet data, so the CMS can scan the images folder and add the images paths to the application data.
		<br><br>Only include one version of each photo with its correct name, and make sure the imagesa exist in the root of the zip file (no folders). 
		<br><br>Any duplicates will be overwritten upon upload.
	</label>

	<br><br>

    <div class="form-group">
        <input type="file" name="userfile" size="20" />
    </div>
    <div class="form-group">
        <input type="hidden" name="uploading" value="true" />
        <input class="btn btn-md btn-primary" type="submit" value="upload" />
    </div>
</form>
