'use strict';

/* Controllers */

almanac.controller('search', ['$scope', '$state', 'Data', 'Linking', function($scope, $state, Data, Linking){

    // name, title,
    $scope.searchField = '';
    $scope.searchIn = '';
    $scope.query = '';

    // Helpers
    $scope.linking = Linking;

    // Data
    $scope.listings = Data.directoryListings;
    $scope.candidates = Data.candidatesArray;
    $scope.senate = Data.rawdata.districts['senate'];
    $scope.house = Data.rawdata.districts['house'];
    $scope.congress = Data.rawdata.districts['congress'];
    $scope.judicial = Data.rawdata.districts['judicial'];


    $scope.searchAll = function(item){
        if($scope.searchField && $scope.activeSearch){
            if(item[$scope.searchField] != undefined){
                if(typeof(item[$scope.searchField]) == 'string'){
                    return item[$scope.searchField].toLowerCase().indexOf($scope.activeSearch.toLowerCase())!=-1;
                } else {
                    var matches = false;
                    $.each(item[$scope.searchField], function(i, v){;
                       if(v.name.toLowerCase().indexOf($scope.activeSearch.toLowerCase())!=-1){
                           matches = true;
                           item.matchedCommittee = v.name;
                       }
                    });
                    return matches;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    $scope.checkSearch = function($event){
        if($event.keyCode == 13){
            $scope.activeSearch = $scope.query;
        }
    }

}]);