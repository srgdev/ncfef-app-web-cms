'use strict';

/* Controllers */

almanac.controller('directories', ['$scope', '$rootScope', '$state', '$stateParams', 'Data', 'Linking', function($scope, $rootScope, $state, $stateParams, Data, Linking){

    $scope.filters = {type: 'us_congress, ussenate'};
    $scope.query = '';

    // Preformatted array of directories concatenated
    $scope.listings = Data.directoryListings;
    $scope.linking = Linking;

    $scope.filterByType = function(listing){
        if($scope.filters.type != null){
            // If multiple comparators
            if($scope.filters.type.indexOf(',') !== -1){
                var filters = $scope.filters.type.split(',');
                var match = false;
                $.each(filters, function(i, v){
                    if (v == listing.type) {
                        match = true;
                    }
                })
                return match;
            } else {
                return ($scope.filters.type == listing.type);
            }    
        } else {
            return listing;
        }
        
        
    }

    $scope.clearType = function(){
        delete $scope.filters.type;
    }

    $scope.searchFields = function(item){
        if(!item.DirectoryName){
            return false;
        }
        return !!( item.DirectoryName.toLowerCase().indexOf($scope.query.toLowerCase() || '')!=-1 || (item.District && item.District.indexOf($scope.query || '')!=-1) || item['type'].indexOf($scope.query || '')!=-1 );
    }

}]);