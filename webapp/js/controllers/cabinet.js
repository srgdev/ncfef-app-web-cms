'use strict';

/* Controllers */

almanac.controller('cabinet', ['$scope', '$rootScope', '$state', '$stateParams', 'Data', function($scope, $rootScope, $state, $stateParams, Data){
    // Get raw listing data using state url parameters dirId
    $scope.listing = Data.rawdata.directory.cabinet[$stateParams.dirId];
}]);